import React, {MouseEventHandler, useEffect, useRef} from "react";
import clsx from "clsx";
import {CellValueEnum} from "../../../../core/constants/enums";
import styles from './styles.module.css'

type ManualCellProps = {
    value: CellValueEnum,
    rowIndex: number,
    columnIndex: number,
    onEnter?: (rowIndex: number, columnIndex: number) => void,
    onLeave?: (rowIndex: number, columnIndex: number) => void,
}


type EmptyManualCellProps = {
    rowIndex: number,
    columnIndex: number,
    onEnter?: (rowIndex: number, columnIndex: number) => void,
    onLeave?: (rowIndex: number, columnIndex: number) => void,
}
const EmptyManualCell: React.FC<EmptyManualCellProps> = ({rowIndex, columnIndex, onEnter, onLeave}) => {
    const onMouseEnter: MouseEventHandler<HTMLDivElement> = (e) => {
        onEnter?.(rowIndex, columnIndex)
    }
    const onMouseLeave: MouseEventHandler<HTMLDivElement> = (e ) => {
        onLeave?.(rowIndex, columnIndex)
    }
    return <div onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} className={clsx(styles.cell)}/>
}

const HoveredManualCell: React.FC<EmptyManualCellProps> = ({rowIndex, columnIndex, onEnter, onLeave}) => {
    const onMouseEnter: MouseEventHandler<HTMLDivElement> = (e) => {
        onEnter?.(rowIndex, columnIndex)
    }
    const onMouseLeave: MouseEventHandler<HTMLDivElement> = (e ) => {
        onLeave?.(rowIndex, columnIndex)
    }
    return <div onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} className={clsx(styles.cell, styles.hoveredManualCell)}/>
}

const ShipCell = () => {
    return <div className={clsx(styles.cell, styles.shipCell)}/>
}

const ManualBlockedCell = () => <div className={clsx(styles.cell, styles.blockedCell)}/>

export const ManualCell: React.FC<ManualCellProps> = ({value, rowIndex, columnIndex, onEnter, onLeave}) => {
    switch (value) {
        case CellValueEnum.empty:
            return <EmptyManualCell onEnter={onEnter} onLeave={onLeave} rowIndex={rowIndex} columnIndex={columnIndex}/>
        case CellValueEnum.miss:
            return <ManualBlockedCell/>
        case CellValueEnum.ship:
            return <ShipCell/>
        case CellValueEnum.kill:
            return <HoveredManualCell onEnter={onEnter} onLeave={onLeave} rowIndex={rowIndex} columnIndex={columnIndex}/>
        default:
            return <EmptyManualCell onEnter={onEnter} onLeave={onLeave} rowIndex={rowIndex} columnIndex={columnIndex}/>
    }
}