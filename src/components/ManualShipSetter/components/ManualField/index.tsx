import React from "react";
import {LeftColumnValues} from "../../../../core/constants";
import {HeaderCell} from "../../../Field/components/Cell";
import {HeaderRow} from "../../../Field/components/HeaderRow";
import {CellValueEnum} from "../../../../core/constants/enums";
import {ManualCell} from "../ManualCell";
import styles from "./styles.module.css";

type ManualFieldTypes = {
    field: Array<Array<CellValueEnum>>,
    onEnter?: (rowIndex: number, columnIndex: number) => void,
    onLeave?: (rowIndex: number, columnIndex: number) => void,
    fieldRef: any
}

export const ManualField: React.FC<ManualFieldTypes> = ({
                                                            field,
                                                            onLeave,
                                                            onEnter,
                                                            fieldRef,
                                                        }) => {
    return <div className={styles.fieldContainer}>
        <div style={{marginTop: 30}}>
            {LeftColumnValues.map(val => <HeaderCell key={val} value={val}/>)}
        </div>
        <div>
            <HeaderRow/>
            <div ref={fieldRef} className={styles.fieldBackground}>
                {field.map((row, rowIndex) => <div key={`ManualField__Row-${rowIndex}`} className={styles.fieldRow}>
                        {row.map((cell, i) => <ManualCell
                                key={`ManualField__Row-${rowIndex}__Column-${i}-`}
                                rowIndex={rowIndex}
                                onEnter={onEnter}
                                onLeave={onLeave}
                                columnIndex={i}
                                value={cell}
                            />
                        )}
                    </div>
                )}
            </div>
        </div>
    </div>
}