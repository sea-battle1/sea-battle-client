import React, {useEffect, useRef} from "react";
import clsx from "clsx";
import {RetweetOutlined} from '@ant-design/icons';
import { disablePageScroll, enablePageScroll } from 'scroll-lock';
import {SelfCell} from "../../../Field/components/Cell";
import {CellValueEnum} from "../../../../core/constants/enums";
import {getArr, getCurrentShipCellIndex} from "./utils";
import styles from './styles.module.css'

export type ShipMoveType = {
    isMouseDown: boolean,
    x: number,
    y: number,
    currentIndexCell: number
    cellCount: number
    isVertical: boolean
}

type ShipProps = {
    cellCount: number,
    isMouseDown: boolean,
    setShipMoveConfig: (value: ShipMoveType) => void,
    setCurrentRefMovedShip: (ref: any) => void,
    isVertical?: boolean,
    setIsVertical?: (value: boolean) => void
}

export const Ship: React.FC<ShipProps> = ({cellCount, setShipMoveConfig, isMouseDown, setCurrentRefMovedShip, isVertical, setIsVertical}) => {
    const shipRef = useRef<HTMLDivElement>(null)
    const shipCellsRef = useRef<HTMLDivElement>(null)

    const arr = getArr(cellCount)

    useEffect(() => {
        const downHandler = (e: MouseEvent) => {
            setCurrentRefMovedShip(shipRef)
            if (shipRef.current) {
                const coords = shipRef.current.getBoundingClientRect()
                const Y = coords.top
                const X = coords.left
                setShipMoveConfig({
                    isMouseDown: true,
                    x: e.clientX - X,
                    y: e.clientY - Y,
                    currentIndexCell: getCurrentShipCellIndex(
                        cellCount,
                        isVertical ? coords.height : coords.width - 16,
                        isVertical ? e.clientY - Y : e.clientX - X
                    ),
                    cellCount,
                    isVertical: isVertical || false
                })
                shipRef.current?.classList.add(styles.movedShip)
                shipRef.current.style.top = `${Y}px`
                shipRef.current.style.left = `${X}px`
            }
        }

        const touchStartHandler = (e: TouchEvent) => {
            setCurrentRefMovedShip(shipRef)
            if (shipRef.current) {
                disablePageScroll()
                const coords = shipRef.current.getBoundingClientRect()
                const Y = coords.top
                const X = coords.left
                setShipMoveConfig({
                    isMouseDown: true,
                    x: e.touches[0].clientX - X,
                    y: e.touches[0].clientY - Y,
                    currentIndexCell: getCurrentShipCellIndex(
                        cellCount,
                        isVertical ? coords.height : coords.width - 16,
                        isVertical ? e.touches[0].clientY - Y : e.touches[0].clientX - X
                    ),
                    cellCount,
                    isVertical: isVertical || false
                })
                shipRef.current?.classList.add(styles.movedShipTouch)
                shipRef.current.style.top = `${Y}px`
                shipRef.current.style.left = `${X}px`
            }
        }
        if (shipCellsRef.current) {
            shipCellsRef.current.addEventListener('mousedown', downHandler)
            shipCellsRef.current.addEventListener('touchstart', touchStartHandler)
            // @ts-ignore
        }
        return () => {
            if (shipCellsRef.current) {
                shipCellsRef.current.removeEventListener('mousedown', downHandler)
                shipCellsRef.current.removeEventListener('touchstart', touchStartHandler)
            }
        }
    }, [isVertical])

    return <div
        ref={shipRef}
        className={styles.container}
    >
        <div
            ref={shipCellsRef}
            className={clsx(styles.manualShip, {
                [styles.isVertical]: isVertical,
                [styles.isHorisontal]: !isVertical,
            })}
        >
            {arr.map((num, i) => <SelfCell key={`${num}-${i}`} value={CellValueEnum.ship} columnIndex={num} rowIndex={i}/>)}
        </div>
        {cellCount !== 1 && <RetweetOutlined onClick={() => {
            // @ts-ignore
            setIsVertical(!isVertical)
        }} className={styles.rotateIcon}/>}
    </div>
}