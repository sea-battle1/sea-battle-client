export const getCurrentShipCellIndex = (cellCount: number, maxNum: number, clickPosition: number): number => {
    if (cellCount === 1) return 1;
    const rel = maxNum - (clickPosition - 2)

    if (cellCount === 2) {
        if (rel >= 38) return 1
        return 2
    }
    if (cellCount === 3) {
        if (rel < 35.33) return 3
        if (rel < 70.66) return 2
        return 1
    }

    if (rel < 34) return 4
    if (rel < 68) return 3
    if (rel < 102) return 2
    return 1
}

export const getArr = (num: number): number[] => {
    const res = []
    for (let i = 0; i < num; i++) {
        res.push(i)
    }
    return res
}