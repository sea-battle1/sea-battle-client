import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {FieldType} from "../../core/types/fieldTypes";
import {CellValueEnum} from "../../core/constants/enums";
import {ShipMoveType} from "./components/Ship";

export class ManualFieldConstructor extends FieldConstructor {
    constructor(field?: FieldType) {
        super(field);
    }

    setShipInField () {
        const line: number[][] = []
        this.setField(this.field.map((row, iRow) => {
                return row.map((cell, iCol) => {
                    if (cell === CellValueEnum.kill) {
                        line.push([iRow, iCol])
                        return CellValueEnum.ship
                    }
                    return cell
                })
            }
        ))
        this.setMissValues(line)
    }

    replaceCellsWithOutIgnored (ignoredValue: CellValueEnum, newValue: CellValueEnum) {
        this.setField(this.field.map(row => row.map(cell => {
            if (cell === ignoredValue) return cell
            return newValue
        })))
        return this
    }

    replaceCells (oldValue: CellValueEnum, newValue: CellValueEnum) {
        this.setField(this.field.map((row) => {
                return row.map((cell) => {
                    if (cell === oldValue) {
                        return newValue
                    }
                    return cell
                })
            }
        ))
        return this
    }

    getIsAvailablePosition (hoveredRowIndex: number, hoveredColIndex: number, shipMoveConfig: ShipMoveType) {
        const coordsShip = []
        for (let i = 1; i <= shipMoveConfig.cellCount; i++) {
            if (shipMoveConfig.isVertical) {
                coordsShip.push([hoveredRowIndex + i - shipMoveConfig.currentIndexCell, hoveredColIndex])
            } else {
                coordsShip.push([hoveredRowIndex, hoveredColIndex + i - shipMoveConfig.currentIndexCell])
            }
        }
        const isOverField = coordsShip.some(coors => coors.some(pos => {
            if (pos < 0 || pos > 9) {
                return true
            }
            return false
        }))

        if (isOverField) return false

        const isSettedShip = coordsShip.some(([rowI, colI]) => {
            if (this.field[rowI][colI] === CellValueEnum.ship || this.field[rowI][colI] === CellValueEnum.miss) {
                return true
            }
            return false
        })

        if (isSettedShip) return false

        return coordsShip
    }

    getHoveredField (hoveredRowIndex: number, hoveredColIndex: number, shipMoveConfig: ShipMoveType) {
        if (shipMoveConfig.cellCount === 1) {
            this.setField(this.field.map((row, index) => {
                if (index === hoveredRowIndex) {
                    return row.map((col, colIndex) => {
                        if (colIndex === hoveredColIndex) {
                            return CellValueEnum.kill
                        } else if (col === CellValueEnum.ship) {
                            return CellValueEnum.ship
                        } else if (col === CellValueEnum.miss) {
                            return col
                        }
                        return CellValueEnum.empty
                    })
                }
                return row.map(cell => {
                    if (cell === CellValueEnum.ship) return cell
                    if (cell === CellValueEnum.miss) return cell
                    return CellValueEnum.empty
                })
            }))
        } else {
            const hoveredCells = this.getIsAvailablePosition(hoveredRowIndex, hoveredColIndex, shipMoveConfig)
            if (hoveredCells) {
                this.setField(this.field.map((row, rowI) => {
                    return row.map((cell, colI) => {
                        if (cell === CellValueEnum.ship) return cell
                        if (cell === CellValueEnum.miss) return cell

                        const isHoveredCell = hoveredCells.some(([row, col]) => {
                            return row === rowI && col === colI
                        })
                        if (isHoveredCell) {
                            return CellValueEnum.kill
                        }

                        return CellValueEnum.empty
                    })
                }))
            } else {
                this.setField(this.field.map((row) => {
                    return row.map((cell) => {
                        if (cell === CellValueEnum.ship) return cell
                        if (cell === CellValueEnum.miss) return cell

                        return CellValueEnum.empty
                    })
                }))
            }
        }
        return this
    }

}