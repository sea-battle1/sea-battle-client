import React, {useEffect, useRef, useState} from "react";
import clsx from "clsx";
import {Button, Modal} from "antd";
import {enablePageScroll} from "scroll-lock";
import {CellValueEnum} from "../../core/constants/enums";
import {Ship, ShipMoveType} from "./components/Ship";
import {ManualField} from "./components/ManualField";
import {ManualFieldConstructor} from "./ManualFieldConstructor";
import shipStyles from './components/Ship/styles.module.css'
import styles from './styles.module.css'

type ManualShipSetterProps = {
    onOk: (field: CellValueEnum[][]) => void
    setIsOpen: (val: boolean) => void
    isOpen: boolean
}

export const ManualShipSetter: React.FC<ManualShipSetterProps> = ({onOk, isOpen, setIsOpen}) => {
    const [field, setField] = useState<ManualFieldConstructor>(new ManualFieldConstructor())
    const [shipMoveConfig, setShipMoveConfig] = useState<ShipMoveType>({
        isMouseDown: false,
        x: 0,
        y: 0,
        currentIndexCell: 0,
        cellCount: 0,
        isVertical: false
    })
    const [currentRefMovedShip, setCurrentRefMovedShip] = useState<any>(false)
    const [directionConfig, setDirectionConfig] = useState({
        '4': false,
        '31': false,
        '32': false,
        '21': false,
        '22': false,
        '23': false,
        '11': false,
        '12': false,
        '13': false,
        '14': false,
    })
    const modalBodyRef = useRef<HTMLDivElement>(null)
    const fieldRef = useRef<HTMLDivElement>(null)

    const ships = field.liveShips

    const onCellEnter = (row: number, col: number) => {
        setField(new ManualFieldConstructor(field.getHoveredField(row, col, shipMoveConfig).field))
    }

    const onCellLeave = () => {
        setField(new ManualFieldConstructor(field.replaceCells(CellValueEnum.kill, CellValueEnum.empty).field))
    }

    useEffect(() => {
        const mouseMoveListener = (e: MouseEvent) => {
            const Y = e.clientY - shipMoveConfig.y
            const X = e.clientX - shipMoveConfig.x
            currentRefMovedShip.current.style.top = `${Y}px`
            currentRefMovedShip.current.style.left = `${X}px`
        }
        let lastTouchedCellCoords: any = {
            rowIndex: null,
            colIndex: null
        }
        const touchMoveListener = (e: TouchEvent) => {
            if (shipMoveConfig.isMouseDown) {
                const Y = e.touches[0].clientY - shipMoveConfig.y
                const X = e.touches[0].clientX - shipMoveConfig.x
                if (currentRefMovedShip.current) {
                    currentRefMovedShip.current.style.top = `${Y}px`
                    currentRefMovedShip.current.style.left = `${X}px`
                }
                const fieldCoordsInPage = fieldRef.current?.getBoundingClientRect()
                // @ts-ignore
                const {left, top, width, height} = fieldCoordsInPage
                if (e.touches[0].clientY > top && e.touches[0].clientY < top + width && e.touches[0].clientX > left && e.touches[0].clientX < left + width) {
                    const currentRowIndex = Math.floor(((e.touches[0].clientY - top) / height) * 10)
                    const currentColIndex = Math.floor(((e.touches[0].clientX - left) / width) * 10)
                    if (lastTouchedCellCoords.rowIndex !== currentRowIndex || lastTouchedCellCoords.colIndex !== currentColIndex) {
                        if (field.field[currentRowIndex][currentColIndex] === CellValueEnum.empty || field.field[currentRowIndex][currentColIndex] === CellValueEnum.kill
                        ) {
                            onCellEnter(currentRowIndex, currentColIndex)
                            if (field.getIsAvailablePosition(currentRowIndex, currentColIndex, shipMoveConfig)) {
                                currentRefMovedShip.current?.classList.add(styles.movedShipOpacity)
                            } else {
                                currentRefMovedShip.current?.classList.remove(styles.movedShipOpacity)
                            }
                        } else if (field.field[currentRowIndex][currentColIndex] === CellValueEnum.miss || field.field[currentRowIndex][currentColIndex] === CellValueEnum.ship) {
                            currentRefMovedShip.current?.classList.remove(styles.movedShipOpacity)
                        }

                        lastTouchedCellCoords.rowIndex = currentRowIndex
                        lastTouchedCellCoords.colIndex = currentColIndex
                    }
                } else if (e.touches[0].clientY < top || e.touches[0].clientY > top + width || e.touches[0].clientX < left || e.touches[0].clientX > left + width) {
                    onCellLeave()
                    currentRefMovedShip.current?.classList.remove(styles.movedShipOpacity)

                }
            }
        }

        if (shipMoveConfig.isMouseDown) {
            document.addEventListener('mousemove', mouseMoveListener)
            // @ts-ignore
            document.addEventListener('touchmove', touchMoveListener)
        } else {
            document.removeEventListener('mousemove', mouseMoveListener)
            // @ts-ignore
            document.removeEventListener('touchmove', touchMoveListener)
        }
        return () => {
            document.removeEventListener('mousemove', mouseMoveListener)
            // @ts-ignore
            document.removeEventListener('touchmove', touchMoveListener)
        }
    }, [shipMoveConfig, currentRefMovedShip, field])

    useEffect(() => {
        const mouseUpListener = () => {
            enablePageScroll()
            if (shipMoveConfig.isMouseDown && currentRefMovedShip.current) {
                field.setShipInField()
                setField(new ManualFieldConstructor(field.field))
                currentRefMovedShip.current.classList.remove(shipStyles.movedShip)
                currentRefMovedShip.current?.classList.remove(styles.movedShipOpacity)
                currentRefMovedShip.current?.classList.remove(shipStyles.movedShipTouch)
                setShipMoveConfig({
                    isMouseDown: false,
                    x: 0,
                    y: 0,
                    currentIndexCell: 0,
                    cellCount: 0,
                    isVertical: false
                })
            }
        }
        if (shipMoveConfig.isMouseDown) {
            document.addEventListener('mouseup', mouseUpListener)
            document.addEventListener('touchend', mouseUpListener)
        } else {
            document.removeEventListener('mouseup', mouseUpListener)
            document.removeEventListener('touchend', mouseUpListener)
        }
        return () => {
            document.removeEventListener('mouseup', mouseUpListener)
            document.removeEventListener('touchend', mouseUpListener)
        }
    }, [shipMoveConfig, currentRefMovedShip, field])

    if (!isOpen) return null

    return <Modal
        title="Установите корабли"
        open={isOpen}
        onCancel={() => setIsOpen(false)}
        width={700}
        wrapClassName={styles.manualModal}
    >
        <div className={styles.modalBodyContainer}>
        <div
            ref={modalBodyRef}
            className={clsx(styles.manualShipSetterContainer, {[styles.isMouseDown]: shipMoveConfig.isMouseDown})}
        >
            <ManualField
                field={field.field}
                fieldRef={fieldRef}
                onEnter={shipMoveConfig.isMouseDown ? onCellEnter : undefined}
                onLeave={shipMoveConfig.isMouseDown ? onCellLeave : undefined}
            />
            <div className={styles.shipsBlockContainer}>
                {ships['4'] !== 1 && <div className={clsx(styles.threeCellShips, {
                    [styles.isVerticalFour]: directionConfig["4"]
                })}>
                    {ships['4'] === 0 && <div style={{left: '0%'}}>
                        <Ship isVertical={directionConfig['4']}
                              setIsVertical={(value: boolean) => setDirectionConfig({...directionConfig, '4': value})}
                              setCurrentRefMovedShip={setCurrentRefMovedShip}
                              isMouseDown={shipMoveConfig.isMouseDown}
                              setShipMoveConfig={setShipMoveConfig}
                              cellCount={4}/>
                    </div>}
                </div>}
                {ships['3'] !== 2 && <div className={clsx(styles.threeCellShips, {
                    [styles.isVerticalThree]: directionConfig["31"] || directionConfig["32"]
                })}>
                    {ships['3'] < 2 && <div style={{left: '0%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                       isMouseDown={shipMoveConfig.isMouseDown}
                                                                       setShipMoveConfig={setShipMoveConfig}
                                                                       cellCount={3}
                                                                       isVertical={directionConfig['31']}
                                                                       setIsVertical={(value: boolean) => setDirectionConfig({
                                                                           ...directionConfig,
                                                                           '31': value
                                                                       })}
                    /></div>}
                    {ships['3'] === 0 &&
                        <div style={{left: '51%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                         isMouseDown={shipMoveConfig.isMouseDown}
                                                         setShipMoveConfig={setShipMoveConfig}
                                                         cellCount={3}
                                                         isVertical={directionConfig['32']}
                                                         setIsVertical={(value: boolean) => setDirectionConfig({
                                                             ...directionConfig,
                                                             '32': value
                                                         })}
                        /></div>}
                </div>}
                {ships['2'] !== 3 && <div className={clsx(styles.twoCellShips, {
                    [styles.isVerticalTwo]: directionConfig["21"] || directionConfig["22"] || directionConfig["23"]
                })}>
                    {ships['2'] < 3 && <div style={{left: '0%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                       isMouseDown={shipMoveConfig.isMouseDown}
                                                                       setShipMoveConfig={setShipMoveConfig}
                                                                       cellCount={2}
                                                                       isVertical={directionConfig['21']}
                                                                       setIsVertical={(value: boolean) => setDirectionConfig({
                                                                           ...directionConfig,
                                                                           '21': value
                                                                       })}
                    /></div>}
                    {ships['2'] < 2 && <div style={{left: '33%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                        isMouseDown={shipMoveConfig.isMouseDown}
                                                                        setShipMoveConfig={setShipMoveConfig}
                                                                        cellCount={2}
                                                                        isVertical={directionConfig['22']}
                                                                        setIsVertical={(value: boolean) => setDirectionConfig({
                                                                            ...directionConfig,
                                                                            '22': value
                                                                        })}
                    /></div>}
                    {ships['2'] === 0 &&
                        <div style={{left: '66%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                         isMouseDown={shipMoveConfig.isMouseDown}
                                                         setShipMoveConfig={setShipMoveConfig}
                                                         cellCount={2}
                                                         isVertical={directionConfig['23']}
                                                         setIsVertical={(value: boolean) => setDirectionConfig({
                                                             ...directionConfig,
                                                             '23': value
                                                         })}
                        /></div>}
                </div>}
                {ships['1'] !== 4 && <div className={styles.oneCellShips}>
                    {ships['1'] < 4 && <div style={{left: '0%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                       isMouseDown={shipMoveConfig.isMouseDown}
                                                                       setShipMoveConfig={setShipMoveConfig}
                                                                       cellCount={1}/></div>}
                    {ships['1'] < 3 && <div style={{left: '21%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                        isMouseDown={shipMoveConfig.isMouseDown}
                                                                        setShipMoveConfig={setShipMoveConfig}
                                                                        cellCount={1}/></div>}
                    {ships['1'] < 2 && <div style={{left: '42%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                                        isMouseDown={shipMoveConfig.isMouseDown}
                                                                        setShipMoveConfig={setShipMoveConfig}
                                                                        cellCount={1}/></div>}
                    {ships['1'] < 1 &&
                        <div style={{left: '63%'}}><Ship setCurrentRefMovedShip={setCurrentRefMovedShip}
                                                         isMouseDown={shipMoveConfig.isMouseDown}
                                                         setShipMoveConfig={setShipMoveConfig}
                                                         cellCount={1}/></div>}
                </div>}
            </div>
        </div>
        <div className={styles.buttonContainer}>
            <Button onClick={() => setField(new ManualFieldConstructor())} type={"default"}
                    className={styles.clearButton}>Очистить поле</Button>
            <div className={styles.rightBtnsBlock}>
                <Button onClick={() => setIsOpen(false)} danger>Отменить</Button>
                <Button disabled={!field.isAllShipsInField} onClick={() => {
                    onOk(field.replaceCellsWithOutIgnored(CellValueEnum.ship, CellValueEnum.empty).field)
                    setField(new ManualFieldConstructor())
                }} type="primary">Подтвердить</Button>
            </div>
        </div>
    </div>
    </Modal>
}
