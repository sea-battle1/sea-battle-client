import React from "react";
import clsx from "clsx";
import {KilledShip} from "./KilledShip";
import {ShipsType} from "../../core/helpers/FieldConstructor";
import styles from './styles.module.css'

type KilledShipsBlockProps = {
    killedShips: ShipsType
    isRight?: boolean
}

export const KilledShipsBlock: React.FC<KilledShipsBlockProps> = ({isRight, killedShips}) => {
    return <div className={clsx({[styles.isMarginLeft]: isRight})} >
        <div className={styles.killedShipsContainer}>
            <div className={styles.line}>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 1}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 2}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 3}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] === 4}/>
            </div>
            <div className={styles.line}>
                <KilledShip cellCount={2} isKilled={killedShips['2'] >= 1}/>
                <KilledShip cellCount={2} isKilled={killedShips['2'] >= 2}/>
                <KilledShip cellCount={2} isKilled={killedShips['2'] === 3}/>
            </div>
            <div className={styles.line}>
                <KilledShip cellCount={3} isKilled={killedShips['3'] >= 1}/>
                <KilledShip cellCount={3} isKilled={killedShips['3'] === 2}/>
            </div>
            <div className={styles.line}>
                <KilledShip cellCount={4} isKilled={killedShips['4'] === 1}/>
            </div>
        </div>
    </div>
}