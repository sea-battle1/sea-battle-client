import React from "react";
import {CellValueEnum} from "../../../core/constants/enums";
import {SelfCell} from "../../Field/components/Cell";
import styles from './styles.module.css'

type KilledShipProps = {
    cellCount: number,
    isKilled: boolean
}

const getArr = (num: number): number[] => {
    const res = []
    for (let i = 0; i < num; i++) {
        res.push(i)
    }
    return res
}

export const KilledShip: React.FC<KilledShipProps> = ({cellCount, isKilled}) => {

    const arr = getArr(cellCount)


    return <div className={styles.shipContainer}>
        <div className={styles.ship}>
            {arr.map(num =>  <SelfCell key={num} rowIndex={num} columnIndex={num} value={isKilled ? CellValueEnum.kill : CellValueEnum.ship}/>)}
        </div>
    </div>
}