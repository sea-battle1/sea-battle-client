import React from "react";
import ReactDom from "react-dom";
import clsx from "clsx";
import {Button, Statistic} from "antd";
import styles from "./styles.module.css";

const { Countdown } = Statistic;

type FindGameModal = {
    isOpen: boolean,
    onClickBtn: () => void
}

export const FindGameModal: React.FC<FindGameModal> = ({isOpen, onClickBtn}) => {
    if (!isOpen) return null
    return <>{ReactDom.createPortal(<aside className={styles.findGameModalContainer}>
        <section className={styles.findGameModalBody}>
            <h6 className={styles.loadingMessage}>Идет поиск игроков...
                <Countdown
                    className={styles.findGameTimer}
                    title=""
                    value={Date.now() + 30 * 1000}
                    onFinish={onClickBtn}
                    format="mm:ss"
                />
            </h6>
            <div className={clsx("loaderElement", styles.loading)}></div>
            <Button className={styles.playToBotBtn} onClick={onClickBtn}>Играть с компьютером</Button>
        </section>
    </aside>, document.getElementById('root') as HTMLElement)}</>
}