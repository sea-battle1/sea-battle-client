import React, {useEffect, useRef} from "react";
import clsx from "clsx";
import gsap from 'gsap'
import {CellValueEnum} from "../../../../core/constants/enums";
import {CloseIcon} from "../../../../core/assets/icons/CloseIcon";
import styles from './styles.module.css'


type HeaderCellTypes = {
    value?: string | number,
}

export const HeaderCell: React.FC<HeaderCellTypes> = ({value}) => {
    return <div className={styles.headerCell}>{value}</div>
}

type CellTypes = {
    value: CellValueEnum,
    columnIndex: number,
    rowIndex: number,
    onClickEmptyCell?: (row: number, col: number) => void,
    showAnimate?: boolean
}

type EmptyCellTypes = {
    rowIndex: number,
    isNotClickable?: boolean,
    columnIndex: number,
    onClickEmptyCell?: (row: number, col: number) => void
}

const EmptyCell: React.FC<EmptyCellTypes> = ({rowIndex, columnIndex, isNotClickable, onClickEmptyCell}) => {
    if (isNotClickable) return <div className={styles.cell}/>
    return <div onClick={() => onClickEmptyCell?.(rowIndex, columnIndex)} className={clsx(styles.cell, styles.emptyCell)}/>
}

const MissCell = () => {
    return <div className={clsx(styles.cell, styles.missCell)}>
        <div className={styles.point}/>
    </div>
}

const ShipCell = () => {
    return <div className={clsx(styles.cell, styles.shipCell)}/>
}

type KillCellProps = {
    showAnimate?: boolean
}

const KillCell: React.FC<KillCellProps> = ({showAnimate}) => {
    const cellRef = useRef(null)
    useEffect(() => {
        if (showAnimate) {
            gsap.from(cellRef.current, {
                boxShadow: '0 0 40px 40px rgba(0,0,0,0.5)',
                duration: 0.5,
            })
        }
    }, [])
    return <div ref={cellRef} className={clsx(styles.cell, styles.killCell)}><CloseIcon/></div>
}

export const EnemyCell: React.FC<CellTypes> = ({value, rowIndex, columnIndex, onClickEmptyCell}) => {
    switch (value) {
        case CellValueEnum.empty:
            return <EmptyCell rowIndex={rowIndex} columnIndex={columnIndex} onClickEmptyCell={onClickEmptyCell} />
        case CellValueEnum.miss:
            return <MissCell/>
        case CellValueEnum.ship:
            return <EmptyCell rowIndex={rowIndex} columnIndex={columnIndex}/>
        case CellValueEnum.kill:
            return <KillCell/>
        default:
            return <EmptyCell rowIndex={rowIndex} columnIndex={columnIndex}/>
    }
}

export const SelfCell: React.FC<CellTypes> = ({value, rowIndex, columnIndex, showAnimate}) => {
    switch (value) {
        case CellValueEnum.empty:
            return <EmptyCell isNotClickable rowIndex={rowIndex} columnIndex={columnIndex}/>
        case CellValueEnum.miss:
            return <MissCell/>
        case CellValueEnum.ship:
            return <ShipCell/>
        case CellValueEnum.kill:
            return <KillCell showAnimate={showAnimate}/>
        default:
            return <EmptyCell isNotClickable rowIndex={rowIndex} columnIndex={columnIndex}/>
    }
}