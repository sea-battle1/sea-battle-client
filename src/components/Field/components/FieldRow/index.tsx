import React from "react";
import {EnemyCell, SelfCell} from "../Cell";
import {CellValueEnum, FieldTypesEnum} from "../../../../core/constants/enums";
import styles from './styles.module.css'

type FieldRowTypes = {
    row: Array<CellValueEnum>,
    rowIndex: number,
    fieldType: FieldTypesEnum,
    onClickEmptyCell?: (row: number, col: number) => void,
}

export const FieldRow: React.FC<FieldRowTypes> = ({row, rowIndex, fieldType, onClickEmptyCell }) => {
    return <div className={styles.fieldRow}>
        {row.map((cell, i) => {
            if (fieldType === FieldTypesEnum.self) {
                return <SelfCell showAnimate rowIndex={rowIndex} columnIndex={i} key={`${fieldType}-${i}-${rowIndex}`}
                             value={cell}/>
            } else {
                return <EnemyCell onClickEmptyCell={onClickEmptyCell} rowIndex={rowIndex} columnIndex={i} key={`${fieldType}-${i}-${rowIndex}`}
                             value={cell}/>
            }
        })}
    </div>
}
