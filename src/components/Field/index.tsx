import React, {MouseEventHandler, useLayoutEffect, useState} from "react";
import clsx from "clsx";
import {FieldRow} from "./components/FieldRow";
import {HeaderRow} from "./components/HeaderRow";
import {LeftColumnValues} from "../../core/constants";
import {HeaderCell} from "./components/Cell";
import {CellValueEnum, FieldTypesEnum} from "../../core/constants/enums";
import {FieldHeader} from "../FieldHeader";
import {FieldConstructor, ShipsType} from "../../core/helpers/FieldConstructor";
import {Button} from "antd";
import {ManualShipSetter} from "../ManualShipSetter";
import {useWindowsWidth} from "../../core/helpers/useWindowWidth";
import styles from './styles.module.css'
import gsap from "gsap";
import cellStyles from './components/Cell/styles.module.css'

let tl = gsap.timeline()

type FieldHeaderConfig = {
    fieldKilledShips: ShipsType,
    fieldHeaderColor: 'green' | 'red',
    fieldHeaderName: string,
}

type FieldTypes = {
    field: Array<Array<CellValueEnum>>,
    setField?: (f: FieldConstructor) => void,
    fieldType: FieldTypesEnum,
    fieldHeaderConfig?: FieldHeaderConfig
    isLoading?: boolean
    checkMouse?: boolean
    fieldRef?: any
    cursorRef?: any
    onClickEmptyCell?: (row: number, col: number) => void
    onMoveEnemyField?: (Y: number, X: number, type: string) => void,
    onEnterContainer?: MouseEventHandler<HTMLDivElement>,
    onLeaveContainer?: MouseEventHandler<HTMLDivElement>,
    showReplaceShipBtn?: boolean
    showManualSetShipBtn?: boolean
    gameIsPlaying?: boolean
}

export const Field: React.FC<FieldTypes> = ({
                                                field,
                                                setField,
                                                fieldType,
                                                isLoading,
                                                onClickEmptyCell,
                                                onMoveEnemyField,
                                                checkMouse = false,
                                                fieldRef,
                                                onEnterContainer,
                                                onLeaveContainer,
                                                cursorRef,
                                                fieldHeaderConfig,
                                                showReplaceShipBtn,
                                                showManualSetShipBtn,
                                                gameIsPlaying
                                            }) => {
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

    const width = useWindowsWidth()

    useLayoutEffect(() => {
        if (fieldRef && fieldRef.current && checkMouse && onMoveEnemyField) {
            const listener = (e: MouseEvent) => {
                // @ts-ignore
                const fieldY = e.clientY - Math.floor(+fieldRef.current?.getBoundingClientRect().top as number)
                // @ts-ignore
                const fieldX = e.clientX - Math.floor(+fieldRef.current?.getBoundingClientRect().left as number)
                onMoveEnemyField(fieldY, fieldX, e.type)
            }
            fieldRef.current?.addEventListener('mousemove', listener)
            fieldRef.current?.addEventListener('mouseleave', listener)
            fieldRef.current?.addEventListener('mouseenter', listener)
            return () => {
                fieldRef.current?.removeEventListener('mousemove', listener)
                fieldRef.current?.removeEventListener('mouseleave', listener)
                fieldRef.current?.removeEventListener('mouseenter', listener)
            }
        }
    }, [])

    const zoomValue = (document.body.clientHeight - 436) / 330 < 0.8 ? (document.body.clientHeight - 436) / 330 : 0.8
    const marginValue = document.body.clientHeight - 436 < 300 ? document.body.clientHeight - 436 : 300

    const setRandomField = () => {
        tl.to(`#Field__Self .${cellStyles.shipCell}`, {
            duration: 0.5,
            scale: 2.5,
            opacity: 0,
            rotate: 360,
            onComplete: () => {
                setField?.(new FieldConstructor(new FieldConstructor().getEmptyField()))
                setTimeout(() => {
                    setField?.(new FieldConstructor().getRandomCompletedField())
                    setTimeout(() => {
                        tl.from(`#Field__Self .${cellStyles.shipCell}`, {rotate: 180, scale: 2.5, opacity: 0, duration: 0.3});
                    }, 30)
                }, 10)

            }
        });
    }

    return <div className={styles.fieldContainer}>
        {fieldHeaderConfig && <FieldHeader
            // @ts-ignore
            style={{marginTop: gameIsPlaying && width < 600 && marginValue}}
            className={clsx({
                [styles.playerInfoBlock]: gameIsPlaying,
                [styles.mobileStartGameMargin]: gameIsPlaying && width < 600
            })}
            color={fieldHeaderConfig.fieldHeaderColor}
            name={fieldHeaderConfig.fieldHeaderName}
            killedShips={fieldHeaderConfig.fieldKilledShips}
        />}
        <div
            style={{
                zoom: width < 600 && gameIsPlaying ? zoomValue : 1,
                left: width < 600 && gameIsPlaying ? (1.1 - zoomValue) * 50 : 0
            }}
            className={clsx({
                [styles.mobileStartGame]: width < 600 && gameIsPlaying
            })}>
            <HeaderRow/>
            <div style={{position: 'relative'}}>
                <div className={styles.leftColumn}>
                    {LeftColumnValues.map(val => <HeaderCell key={val} value={val}/>)}
                </div>
                {cursorRef && <div ref={cursorRef} className={styles.cursor}/>}
                <div id={fieldType === FieldTypesEnum.self ? 'Field__Self' : 'Field__Enemy'}
                     onMouseEnter={onEnterContainer} onMouseLeave={onLeaveContainer} ref={fieldRef}
                     className={clsx(styles.fieldBackground, isLoading ? styles.fieldBodyLoading : '')}>
                    {field.map((row, i) => <FieldRow
                        onClickEmptyCell={onClickEmptyCell}
                        fieldType={fieldType}
                        key={`${fieldType}-${i}`}
                        rowIndex={i}
                        row={row}
                    />)}
                </div>
            </div>
            {showReplaceShipBtn && <Button id="RandomShipSetterButton"
                                           className={styles.replaceShipBtn}
                                           onClick={setRandomField}
            >Случайно расположить корабли</Button>}
            {showManualSetShipBtn && <Button id="ManualShipSetterButton"
                                             className={styles.replaceShipBtn}
                                             onClick={() => setIsModalOpen(true)}
            >Расположить корабли вручную</Button>}
        </div>
        <ManualShipSetter
            isOpen={isModalOpen}
            setIsOpen={setIsModalOpen}
            onOk={(field) => {
                setField?.(new FieldConstructor(field))
                setIsModalOpen(false)
            }}
        />
    </div>
}
