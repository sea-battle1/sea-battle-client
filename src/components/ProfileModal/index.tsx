import React, {useEffect, useState} from "react";
import {Modal} from "antd";
import {api} from "../../core/api";
import styles from './styles.module.css'
import {UserType} from "../../core/types/userTypes";
import {TodayGameCount} from "../TodayGameCount";
import {TextBlock} from "./components/TextBlock";

type ProfileModalProps = {
    isOpen: boolean;
    onClose: () => void;
    userId: string | null;
}

export const ProfileModal: React.FC<ProfileModalProps> = ({isOpen, onClose, userId}) => {
    const [userData, setUserData] = useState<UserType | null>(null)
    useEffect(() => {
        (async function () {
            try {
                if (userId && isOpen) {
                    const user = await api.getUserInfo(userId)
                    setUserData(user)
                }
            } catch (err) {
                console.error(err)
            }
        })()
    }, [userId, isOpen])
    return <Modal
        className={styles.profileModal}
        open={isOpen}
        onCancel={onClose}
        onOk={onClose}
        okText="Ок"
        cancelText="Закрыть"
        title={`${userData?.first_name} ${userData?.last_name}`}
    >
        <>
            <TextBlock title="Всего игр" value={userData?.allGames || 0}/>
            <TextBlock title="Всего побед" value={userData?.allWin || 0}/>
            <TextBlock title="Одиночных игр" value={userData?.singleGames || 0}/>
            <TextBlock title="Одиночных побед" value={userData?.singleWin || 0}/>
            <TextBlock title="Многопользовательских игр" value={userData?.mpGames || 0}/>
            <TextBlock title="Многопользовательских побед" value={userData?.mpWin || 0}/>
        </>
    </Modal>
}
