import React from "react";
import styles from './styles.module.css'

type OnlinePlayersCountProps = {
    count: number
}

export const OnlinePlayersCount: React.FC<OnlinePlayersCountProps> = ({count}) => {
    return <div className={styles.todayGameCountContainer}>Игроков онлайн: <span className={styles.countValue}>{count}</span></div>
}
