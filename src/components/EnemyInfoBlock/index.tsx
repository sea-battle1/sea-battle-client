import React from "react";
import styles from './styles.module.css'
import {Tag} from "antd";

export const EnemyInfoBlock: React.FC<any> = ({enemyData}) => {
    if (!enemyData) return null
    return <div className={styles.enemyInfoBlockContainer}>
        <span className={styles.title}>Ваш противник: </span>
        <span className={styles.enemyName}>{enemyData.first_name} {enemyData.last_name}</span>
        <Tag color="#108ee9">Игры: {enemyData.allGames}</Tag>
        <Tag color="#51ba55">Победы: {enemyData.allWin}</Tag>
        <Tag color="#f50">П/И: {enemyData.relativePercent}%</Tag>
    </div>
}