import React, {useEffect, useState} from "react";
import {Modal, Space, Typography} from "antd";
import {AppleDownloadBtn} from "../AppleDownloadBtn";
import {RuStoreDownloadBtn} from "../RuStoreDownloadBtn";
import styles from './styles.module.css'

const { Text, Link } = Typography;
type InfoModalProps = {
    isOpen: boolean;
    onClose: () => void;
}

export const InfoModal: React.FC<InfoModalProps> = ({isOpen, onClose}) => {

    const onClickApple = () => {
        window.open('https://apps.apple.com/ru/app/%D0%BC%D0%BE%D1%80%D1%81%D0%BA%D0%BE%D0%B9-%D0%B1%D0%BE%D0%B9-battleships/id6446815384')
    }

    return <Modal
        className={styles.profileModal}
        open={isOpen}
        onCancel={onClose}
        onOk={onClose}
        okText="Ок"
        cancelText="Закрыть"
        title="Информация"
    ><Space className={styles.content} direction="vertical">
        <Link mark href="https://sea-battles.ru/" target="_blank">Наш сайт</Link>
        <Text>Просьба пройти небольшой опрос в группе:</Text>
        <Link mark href="https://vk.com/public219536338" target="_blank">Группа</Link>
        <div className={styles.mobileLinks}>
            <AppleDownloadBtn onClick={onClickApple}/>
            <RuStoreDownloadBtn/>
        </div>
    </Space>

    </Modal>
}
