import React from "react";
import styles from "./styles.module.css";

type TextBlockProps = {
    title: string,
    value: string | number
}

export const TextBlock: React.FC<TextBlockProps> = ({title, value}) => {
    return <div className={styles.textBlockContainer}>{title}:
        <span className={styles.value}>{value}</span>
    </div>
}
