import React from "react";
// @ts-ignore
import rustoreImg from "../../core/assets/images/rustore_btn.svg";
import styles from './styles.module.css'

export const RuStoreDownloadBtn = () => {
    return <div className={styles.rustoreBtnContainer}>
        <a href="https://apps.rustore.ru/app/com.fristail27.seabattlesmobile" target="_blank">
            <img src={rustoreImg} width="188" height="63" alt="Скачайте из RuStore"/>
        </a>
    </div>
}