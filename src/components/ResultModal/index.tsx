import React, {useEffect, useState} from "react";
import ReactDom from "react-dom";
import bridge from "@vkontakte/vk-bridge";
import * as Sentry from "@sentry/react";
import {Button, message, Statistic, Tag} from "antd";
import {HistoryOutlined, SyncOutlined} from '@ant-design/icons';
import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {GameResultEnum} from "../../core/constants/enums";
import {api} from "../../core/api";
import {ReplayFieldConstructor} from "../../pages/ReplayRecordPage/ReplayFieldConstructor";
import {GameRecordTypes, ReplayRecordType, StrikeType} from "../../core/types/fieldTypes";
import {UserType} from "../../core/types/userTypes";
import {getUserName} from "../../pages/ReplayRecordPage/utils";
import {ReplayFields} from "../../pages/ReplayRecordPage/components/ReplayFields";
import styles from './styles.module.css'

type ResultModalProps = {
    isOpen: boolean,
    showShareBtn?: boolean,
    leftField: FieldConstructor,
    rightField: FieldConstructor,
    gameResult: GameResultEnum,
    onOk: () => void,
    gameConfig?: any,
    gameId?: string | null
    userId?: string | null
}

type ExpandedStrikeType = StrikeType & { side: 'left' | 'right' }


export const ResultModal: React.FC<ResultModalProps> = ({
                                                            isOpen,
                                                            gameResult,
                                                            onOk,
                                                            gameId,
                                                            userId,
                                                            showShareBtn,
                                                        }) => {
    const [gameRecord, setGameRecord] = useState<ReplayRecordType | null>(null)
    const [leftPlayer, setLeftPlayer] = useState<UserType | null>(null)
    const [rightPlayer, setRightPlayer] = useState<UserType | null>(null)
    const [leftField, setLeftField] = useState<ReplayFieldConstructor>()
    const [rightField, setRightField] = useState<ReplayFieldConstructor>()
    const [concatStrikes, setConcatStrikes] = useState<ExpandedStrikeType[]>([])
    const [isEndReplay, setIsEndReplay] = useState<boolean>(false)

    useEffect(() => {
        (async function () {
            try {
                if (gameId && isOpen) {
                    const res = await api.getGameRecord(gameId)
                    if (res.creatorPlayerId !== 'computer') {
                        try {
                            const leftPlayerData = await api.getUserInfo(res.creatorPlayerId)
                            setLeftPlayer(leftPlayerData)
                        } catch (err) {
                            message.open({
                                type: 'error',
                                // @ts-ignore
                                content: 'Не удалось загрузить данные игрока',
                            });
                        }
                    }
                    if (res.secondPlayerId !== 'computer') {
                        try {
                            const rightPlayerData = await api.getUserInfo(res.secondPlayerId)
                            setRightPlayer(rightPlayerData)
                        } catch (err) {
                            message.open({
                                type: 'error',
                                // @ts-ignore
                                content: 'Не удалось загрузить данные игрока',
                            });
                        }
                    }
                    setLeftField(new ReplayFieldConstructor(res.startCreatorPlayerField.map(row => row.map(col => col))))
                    setRightField(new ReplayFieldConstructor(res.startSecondPlayerField.map(row => row.map(col => col))))
                    setGameRecord(res)
                    const leftStrikes: ExpandedStrikeType[] = res?.creatorPlayerStrikes.map(el => ({
                        ...el,
                        side: 'left'
                    })) || []
                    const rightStrikes: ExpandedStrikeType[] = res?.secondPlayerStrikes.map(el => ({
                        ...el,
                        side: 'right'
                    })) || []
                    const unionStrikes = [...leftStrikes, ...rightStrikes].sort((a, b) => a.timeFromStart - b.timeFromStart)
                    setConcatStrikes(unionStrikes)
                    setTimeout(() => {
                        setLeftField(new ReplayFieldConstructor(res?.startCreatorPlayerField.map(row => row.map(col => col))))
                        setRightField(new ReplayFieldConstructor(res?.startSecondPlayerField.map(row => row.map(col => col))))
                        setTimeout(() => {
                            unionStrikes.forEach((strike, i) => {
                                setTimeout(() => {
                                    if (i === unionStrikes.length - 1) {
                                        setIsEndReplay(true)
                                    }
                                    if (strike.side === 'right') {
                                        setLeftField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                                    } else {
                                        setRightField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                                    }
                                }, i * 100)
                            })
                        }, 100)
                    }, 10)

                }
            } catch (err) {
                Sentry.captureException(err);
                message.open({
                    type: 'error',
                    content: 'Не удалось загрузить данные пользователя'
                })
            }
        })()
    }, [isOpen, gameId])

    useEffect(() => {
        if (isEndReplay && isOpen) {
            setIsEndReplay(false)
            setLeftField(new ReplayFieldConstructor(gameRecord?.startCreatorPlayerField.map(row => row.map(col => col))))
            setRightField(new ReplayFieldConstructor(gameRecord?.startSecondPlayerField.map(row => row.map(col => col))))
            concatStrikes.forEach((strike, i) => {
                setTimeout(() => {
                    if (strike.side === 'right') {
                        setLeftField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                    } else {
                        setRightField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                    }
                    if (i === concatStrikes.length - 1) {
                        setIsEndReplay(true)
                    }
                }, i * 100)
            })
        }
    }, [isEndReplay, isOpen, gameId, leftField, rightField])

    if (!isOpen) return null;

    return <>{ReactDom.createPortal(<aside className={styles.resultModalContainer}>
        <section className={styles.resultModalBody}>
            {gameRecord && leftField && rightField && <ReplayFields
                leftHeaderClassName={userId === leftPlayer?.userId ? styles.playerColor : styles.enemyColor}
                rightHeaderClassName={userId === rightPlayer?.userId ? styles.playerColor : styles.enemyColor}
                leftField={leftField}
                rightField={rightField}
                leftPlayerName={getUserName(leftPlayer, gameRecord.creatorPlayerId)}
                rightPlayerName={getUserName(rightPlayer, gameRecord.secondPlayerId)}
                fieldsBlockWidth={780}
            />}
            <div className={styles.statisticsBlock}>
                {gameResult === GameResultEnum.isPlayerWin && <Tag color="green">Победа</Tag>}
                {gameResult === GameResultEnum.isBotWin && <Tag color="red">Поражение</Tag>}
                {gameRecord?.isEndTimeout && <Tag color="cyan-inverse">Окончена по таймауту</Tag>}
                <Tag color={gameRecord?.type === GameRecordTypes.mp ? 'blue' : 'yellow'}>
                    {gameRecord?.type === GameRecordTypes.mp ? 'Многопользовательская' : 'Одиночная'}
                </Tag>
                {gameRecord && <Tag color="purple-inverse">
                    {(((new Date(gameRecord?.gameEndTime).valueOf() || 0) - (new Date(gameRecord?.gameStartTime).valueOf() || 0)) / 1000).toFixed(1)} сек
                </Tag>}
                {gameRecord && <Tag color="green-inverse">
                    Ходов: {(userId === gameRecord?.creatorPlayerId ? gameRecord?.creatorPlayerStrikes.length : gameRecord?.secondPlayerStrikes.length) || 0}
                </Tag>}
                {gameRecord && <Tag color="red-inverse">
                    Ходов: {(userId === gameRecord?.creatorPlayerId ? gameRecord?.secondPlayerStrikes.length : gameRecord?.creatorPlayerStrikes.length) || 0}
                </Tag>}
            </div>
            {gameId && gameRecord && showShareBtn && <Button
                className={styles.showRecordBtn}
                onClick={() => {
                    bridge.send('VKWebAppShowWallPostBox', {
                        message: `Запись боя ${(new Date(gameRecord.gameStartTime)).toLocaleString()}` ,
                        attachments: `https://sea-battles.ru/replayGame/${gameId}`
                    })
                        .then((data) => {
                            if (data.post_id) {
                                // Запись размещена
                            }
                        })
                        .catch(() => {
                            message.error('Не удалось разместить запись')
                        });
                }}
                type="primary">Поделиться боем</Button>}
            <div className={styles.resultModalFooter}>
                <Button type="primary" onClick={() => {
                    setGameRecord(null)
                    setLeftPlayer(null)
                    setRightPlayer(null)
                    setLeftField(undefined)
                    setRightField(undefined)
                    setConcatStrikes([])
                    setIsEndReplay(false)
                    onOk()
                }}>ОК</Button>
            </div>
        </section>
    </aside>, document.getElementById('root') as HTMLElement)}</>
}
