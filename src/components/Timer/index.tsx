import React, {useEffect, useState} from "react";
import styles from './styles.module.css'

type TimerProps = {
    onEndTime?: () => void,
    startTime?: number,
    restartProp: any
}

export const Timer: React.FC<TimerProps> = ({startTime = 30, restartProp, onEndTime}) => {
    const [seconds, setSeconds] = useState<number>(startTime)
    const secondsValue = seconds > 9 ? seconds : `0${seconds}`

    useEffect(() => {
        setSeconds(startTime)
        const interval = setInterval(() => {
            setSeconds((prev) => {
                if (prev - 1 === 0) {
                    clearInterval(interval)
                    onEndTime?.()
                    return prev - 1
                }
                return prev - 1
            })

        }, 1000)
        return () => clearInterval(interval)
    }, [restartProp])

    return <div className={styles.timerContainer}>
        <div className={styles.timer}>
            {seconds === 0 ? 'Время истекло' : `0:${secondsValue}`}
        </div>
    </div>
}