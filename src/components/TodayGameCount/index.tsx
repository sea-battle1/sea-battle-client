import React from "react";
import {Skeleton} from "antd";
import styles from './styles.module.css'

type TodayGameCountProps = {
    count: number,
    isLoadingCount?: boolean
}

export const TodayGameCount: React.FC<TodayGameCountProps> = ({count, isLoadingCount}) => {
    return <div className={styles.todayGameCountContainer}>Игр сегодня:
        <Skeleton className={styles.skeleton} paragraph={{rows: 1, width: 20}} title={false} loading={isLoadingCount} active>
            <span className={styles.countValue}>{count}</span>
        </Skeleton>
    </div>
}
