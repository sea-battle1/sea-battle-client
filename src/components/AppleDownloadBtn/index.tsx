import React from "react";
import {Button} from "antd";
import {AppleLogo} from "../../core/assets/icons/AppleLogo";
import styles from './styles.module.css'

type AppleDownloadBtn = {
    onClick: () => void
}

export const AppleDownloadBtn:React.FC<AppleDownloadBtn> = ({onClick}) => {
    return <Button onClick={onClick} className={styles.btnStyles}>
        <AppleLogo className={styles.logoStyles}/>
        <div className={styles.text}>
            <span>Загрузите в</span>
            <span>App Store</span>
        </div>
    </Button>
}