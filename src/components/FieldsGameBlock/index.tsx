import React, {useRef} from "react";
import {Field} from "../Field";
import {FieldTypesEnum} from "../../core/constants/enums";
import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import styles from './styles.module.css'

type FieldsGameBlockProps = {
    leftField: FieldConstructor,
    rightField: FieldConstructor,
    setLeftField: (field: FieldConstructor) => void,
    gameIsPlaying: boolean,
    onClickEmptyCell: (rowIndex: number, columnIndex: number) => void,
    isLoadingBotField: boolean,
    cursorRef?: any,
    onMoveEnemyField?: (fieldY: number, fieldX: number, type: string) => void
}

export const FieldsGameBlock: React.FC<FieldsGameBlockProps> = ({
                                                                    leftField,
                                                                    setLeftField,
                                                                    gameIsPlaying,
                                                                    onClickEmptyCell,
                                                                    isLoadingBotField,
                                                                    rightField,
                                                                    cursorRef,
                                                                    onMoveEnemyField,
                                                                }) => {
    const enemyRef = useRef<HTMLDivElement>(null)
    return <div className={styles.fieldsContainer}>
        <Field
            fieldType={FieldTypesEnum.self}
            field={leftField.field}
            fieldHeaderConfig={{
                fieldKilledShips: leftField.killedShips,
                fieldHeaderName: "Вы:",
                fieldHeaderColor: "green"
            }}
            setField={setLeftField}
            gameIsPlaying={gameIsPlaying}
            showReplaceShipBtn={!gameIsPlaying}
            showManualSetShipBtn={!gameIsPlaying}
            cursorRef={cursorRef}
        />
        {gameIsPlaying && <Field onClickEmptyCell={onClickEmptyCell}
                                 isLoading={isLoadingBotField}
                                 fieldType={FieldTypesEnum.enemy}
                                 field={rightField.field}
                                 fieldHeaderConfig={{
                                     fieldHeaderColor: "red",
                                     fieldHeaderName: "Противник:",
                                     fieldKilledShips: rightField.killedShips
                                 }}
                                 onMoveEnemyField={onMoveEnemyField}
                                 fieldRef={enemyRef}
                                 checkMouse
        />
        }
    </div>
}