import React from "react";
import {SelfCell} from "../Field/components/Cell";
import {CheckShipType} from "../../core/helpers/FieldConstructor";
import styles from './styles.module.css'

type CheckShipsBlockProps = {
    ships: CheckShipType[][]
}

export const CheckShipsBlock: React.FC<CheckShipsBlockProps> = ({ships}) => {
    return <div className={styles.shipsContainer}>
        {ships.map(ship => <div className={styles.ship}>{ship.map(cell => {
            return <SelfCell rowIndex={cell.row}
                             columnIndex={cell.col}
                             key={`${cell.row}-${cell.col}-${cell.status}`}
                             value={cell.status}/>
        })}</div>)}
    </div>
}