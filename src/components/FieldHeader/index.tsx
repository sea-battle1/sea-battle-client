import React from "react";
import {ShipsType} from "../../core/helpers/FieldConstructor";
import styles from './styles.module.css'
import {KilledShip} from "../KilledShipsBlock/KilledShip";
import clsx from "clsx";

type FieldHeaderProps = {
    name: string,
    className?: string,
    killedShips: ShipsType,
    color: 'red' | 'green',
    style?: React.CSSProperties
}

export const FieldHeader: React.FC<FieldHeaderProps> = ({name, killedShips, color, className, style}) => {
    return <div style={style} id={color === 'green' ? 'FieldHeader__Player' : 'FieldHeader__Enemy' } className={clsx(styles.fieldHeader, className, {
        [styles.greenColorBackground]: color === 'green',
        [styles.redColorBackground]: color === 'red',
    })}>
        <div className={clsx(styles.name)}>{name}</div>
        <div className={styles.shipBlock}>
            <div className={styles.shipLine}>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 1}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 2}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] >= 3}/>
                <KilledShip cellCount={1} isKilled={killedShips['1'] === 4}/>
                <KilledShip cellCount={2} isKilled={killedShips['2'] >= 1}/>
                <KilledShip cellCount={4} isKilled={killedShips['4'] === 1}/>
            </div>
            <div className={styles.shipLine}>
                <KilledShip cellCount={2} isKilled={killedShips['2'] >= 2}/>
                <KilledShip cellCount={2} isKilled={killedShips['2'] === 3}/>
                <KilledShip cellCount={3} isKilled={killedShips['3'] >= 1}/>
                <KilledShip cellCount={3} isKilled={killedShips['3'] === 2}/>
            </div>
        </div>
    </div>
}