import {CellValueEnum} from "../constants/enums";

export type FieldType = CellValueEnum[][]

export enum GameRecordTypes {
    single = 'single',
    mp = 'mp'
}

export type StrikeType = {
    row: number,
    column: number,
    timeFromStart: number
    isEndGame?: boolean
}

export type RecordReplayFromList = {
    gameId: string,
    gameStartTime: number,
    durationGame: number,
    type: GameRecordTypes,
    result: string,
    userSteps: number,
    enemySteps: number,
    userIsWinner: boolean
}

export type ReplayListType = {
    records: RecordReplayFromList[]
}

export type ReplayRecordType = {
    gameId: string,
    type: GameRecordTypes,
    creatorPlayerId: string,
    secondPlayerId: string,
    gameStartTime: number,
    gameEndTime: number,
    startCreatorPlayerField: FieldType,
    startSecondPlayerField: FieldType,
    creatorPlayerStrikes: StrikeType[],
    secondPlayerStrikes: StrikeType[],
    isEndTimeout: boolean,
    winnerId: string
}