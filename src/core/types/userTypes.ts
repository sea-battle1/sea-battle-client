import {AuthTypes} from "./authTypes";

export type UserType = {
    "userId": string,
    "first_name": string,
    "last_name": string,
    "nick_name": string,
    "authType": AuthTypes,
    "singleGames": number,
    "singleWin": number,
    "singleLoose": number,
    simulateGames: number,
    simulateLoose: number,
    simulateWin: number,
    mpGames: number,
    mpLoose: number,
    mpWin: number,
    relativePercent: number,
    allWin: number,
    allGames: number,
    "createdAt": string,
    "updatedAt": string
}

export type ChatUserResultType = {
    position: number,
    user: {
        id: number,
        is_bot: boolean,
        first_name?: string,
        last_name?: string,
        username?: string,
        photoUrl?: string
    },
    score: number
}