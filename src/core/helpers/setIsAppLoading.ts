export const setIsAppLoading = () => {
    const appLoaderScreen = document.getElementById('AppLoaderScreen')
    if (appLoaderScreen) {
        appLoaderScreen.style.display = 'none'
    }
}