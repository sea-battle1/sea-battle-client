import {FieldType} from "../types/fieldTypes";
import {CellValueEnum} from "../constants/enums";

const getRandomBool = ():boolean => {
    return Math.random() > 0.5
}

const getTwoRandomNums = (): number[] => {
    const randArrNum = Math.floor((Math.random() * 100)).toString().split('').map(num => +num)
    if (!Number.isInteger(randArrNum[1])) {
        randArrNum.unshift(0)
    }
    return randArrNum
}

const createCoordinate = (startPoint: number[], lengthShip:number, isVertical:boolean, isPlusCoordinate: boolean ) => {
    const [row, col] = startPoint;

    if (isVertical) {
        if (row < lengthShip - 1) {
            let res = []
            for (let i = 0; i< lengthShip; i++) {
                res.push([row+i, col])
            }
            return res
        } else if (row >= lengthShip - 1 && row <= (9 - lengthShip + 1)) {
            if (isPlusCoordinate) {
                let res = []
                for (let i = 0; i< lengthShip; i++) {
                    res.push([row+i, col])
                }
                return res
            } else {
                let res = []
                for (let i = 0; i< lengthShip; i++) {
                    res.push([row-i, col])
                }
                return res
            }
        } else {
            let res = []
            for (let i = 0; i< lengthShip; i++) {
                res.push([row-i, col])
            }
            return res
        }
    } else {
        if (col < lengthShip - 1) {
            let res = []
            for (let i = 0; i< lengthShip; i++) {
                res.push([row, col+i])
            }
            return res
        } else if (col >= lengthShip - 1 && col <= (9 - lengthShip + 1)) {
            if (isPlusCoordinate) {
                let res = []
                for (let i = 0; i< lengthShip; i++) {
                    res.push([row, col+i])
                }
                return res
            } else {
                let res = []
                for (let i = 0; i< lengthShip; i++) {
                    res.push([row, col-i])
                }
                return res
            }
        } else {
            let res = []
            for (let i = 0; i< lengthShip; i++) {
                res.push([row, col-i])
            }
            return res
        }
    }
}

export type ShipsType = {
    4: number,
    3: number,
    2: number,
    1: number,
}

export type CheckShipType = {
    row: number,
    col: number,
    status: CellValueEnum
}


export class FieldConstructor {
    field: FieldType = this.getEmptyField()
    ships: CheckShipType[][] = []
    counterGeneration: number = 0

    constructor(field?: FieldType) {
        if (field) {
            this.setField(field)
        }
    }

    get isHaveShip () {
        let result = false
        for (let i = 0; i< this.field.length; i++) {
            for (let j = 0; j < this.field[i].length; j++) {
                if (this.field[i][j] === CellValueEnum.ship) {
                    result = true
                    break
                }
            }
            if (result) {
                break
            }
        }
        return result
    }

    get killedShips (): ShipsType {
        const result: ShipsType = {
            '1': 0,
            '2': 0,
            '3': 0,
            '4': 0
        }
         this.ships.forEach((ship) => {
             if (ship.every(cell => cell.status === CellValueEnum.kill) && this.checkShipIsKilled(ship))
                 // @ts-ignore
                 result[ship.length] += 1
         })
         return result
     }

    get liveShips (): ShipsType {
        const result = {
            '1': 0,
            '2': 0,
            '3': 0,
            '4': 0
        }
        this.ships.forEach((ship) => {
            if (ship.every(cell => cell.status === CellValueEnum.ship)) {
                // @ts-ignore
                result[ship.length] += 1
            }
        })
        return result
    }

    get isAllShipsInField (): boolean {
        return (this.liveShips['1'] + this.liveShips['2'] + this.liveShips['3'] + this.liveShips['4']) === 10
    }

    setField (field: FieldType) {
        this.field = field
        this.ships = this.checkLivesShipsInField()
    }

    getEmptyField () {
        return [
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
        ]
    }

    private check_T_L_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (row - 1 < 0 || col - 1 < 0) return defaultValue
        return this.field[row-1][col-1] === value
    }

    private check_T_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (row - 1 < 0) return defaultValue
        return this.field[row-1][col] === value
    }

    private check_T_R_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (row - 1 < 0 || col + 1 > 9) return defaultValue
        return this.field[row-1][col+1] === value
    }

    private check_L_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (col - 1 < 0) return defaultValue
        return this.field[row][col-1] === value
    }

    private check_R_forCell ( row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (col + 1 > 9) return defaultValue
        return this.field[row][col+1] === value
    }

    private check_D_L_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (row + 1 > 9 || col - 1 < 0) return defaultValue
        return this.field[row+1][col-1] === value
    }

    private check_D_forCell (row: number, col: number, value: CellValueEnum, defaultValue = false) {
        if (row + 1 > 9) return defaultValue
        return this.field[row+1][col] === value
    }

    private check_D_R_forCell = (row: number, col: number, value: CellValueEnum, defaultValue = false) => {
        if (row + 1 > 9 || col + 1 > 9) return defaultValue
        return this.field[row+1][col+1] === value
    }

    private checkNotCell (row: number, col: number, value: CellValueEnum) {
        if (col < 0 || col > 9) return true
        if (row < 0 || row > 9) return true
        return this.field[row][col] !== value
    }

    private validateCell (row: number, col: number) {
        if (this.field[row][col] === CellValueEnum.ship) {
            return false
        }
        if (this.field[row][col+1] === CellValueEnum.ship) {
            return false
        }
        if (this.field[row][col-1] === CellValueEnum.ship) {
            return false
        }
        if (this.field[row+1] && (this.field[row+1][col] === CellValueEnum.ship)) {
            return false
        }
        if (this.field[row+1] && (this.field[row+1][col+1] === CellValueEnum.ship)) {
            return false
        }
        if (this.field[row+1] && (this.field[row+1][col-1] === CellValueEnum.ship)) {
            return false
        }
        if (this.field[row-1] && (this.field[row-1][col] === CellValueEnum.ship)) {
            return false
        }
        if (this.field[row-1] && (this.field[row-1][col - 1] === CellValueEnum.ship)) {
            return false
        }
        if (this.field[row-1] && (this.field[row-1][col + 1] === CellValueEnum.ship)) {
            return false
        }

        return true
    }

    private validateShip (coordinates: number[][]) {
        let result = true
        for (let i = 0; i < coordinates.length; i++) {
            const isValidCell = this.validateCell(coordinates[i][0], coordinates[i][1])
            if (!isValidCell) {
                result = false
                break
            }
        }
        return result
    }

    private checkShipIsKilled (ship: CheckShipType[]) {
        return ship.every((cell) => {
            if (cell.status === CellValueEnum.kill &&
                (this.check_D_R_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_D_R_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_D_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_D_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_D_L_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_D_L_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_R_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_R_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_L_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_L_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_T_R_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_T_R_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_T_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_T_forCell(cell.row, cell.col, CellValueEnum.miss, true)) &&
                (this.check_T_L_forCell(cell.row, cell.col, CellValueEnum.kill, true) || this.check_T_L_forCell(cell.row, cell.col, CellValueEnum.miss, true))
            ) return true

            return false
        })
    }

    private createAndCheckShip = (
        startPoints: number[],
        lengthShip:number,
        isVertical:boolean,
        isPlusCoordinate: boolean
    ): number[][] | false => {
        const coordinate = createCoordinate(startPoints, lengthShip, isVertical, isPlusCoordinate)
        const isValid = this.validateShip(coordinate)
        return isValid ? coordinate : false
    }

    private setShip = (coordinates: number[][]) => {
        for (let i = 0; i < coordinates.length; i++) {
            this.field[coordinates[i][0]][coordinates[i][1]] = CellValueEnum.ship
        }
    }

    private addRandomShipInField (lengthShip: number ) {
        this.counterGeneration += 1
        const startPoints = getTwoRandomNums()
        const isVertical = getRandomBool()
        const isPlusCoordinate = getRandomBool()
        const coordinates = this.createAndCheckShip(startPoints, lengthShip, isVertical, isPlusCoordinate)
        if (coordinates) {
            this.setShip(coordinates)
        } else {
            this.addRandomShipInField(lengthShip)
        }
        return this
    }

    private checkCellNeibValues (row: number, col: number, value:CellValueEnum) {
        let result = []
        if (this.field[row][col+1] === value) {
            result.push([row, col + 1])
        }
        if (this.field[row][col-1] === value) {
            result.push([row, col - 1])
        }
        if (this.field[row+1] && (this.field[row+1][col] === value)) {
            result.push([row + 1, col])
        }
        if (this.field[row-1] && (this.field[row-1][col] === value)) {
            result.push([row - 1, col])
        }

        if (result.length > 0) return result

        return false
    }

    private checkCellNeibisNotValues = (row: number, col: number, value:CellValueEnum ) => {
        if (this.field[row][col+1] !== value
            && this.field[row][col-1] !== value
            && (this.field[row-1] ? this.field[row-1][col] !== value : true)
            && (this.field[row+1] ? this.field[row+1][col] !== value : true)
        ) {
            return true
        }
        return false
    }

    private checkRow (row: CellValueEnum[], rowIndex: number, colIndex: number, ship: CheckShipType[]) {
        ship.push({
            row: rowIndex,
            col: colIndex,
            status: row[colIndex]
        })
        row[colIndex] = CellValueEnum.support
        if (colIndex + 1 < 10 && (row[colIndex + 1] === CellValueEnum.ship || row[colIndex + 1] === CellValueEnum.kill)) {
            this.checkRow(row, rowIndex, colIndex + 1, ship)
        }
    }

    private checkColumn (field: CellValueEnum[][], rowIndex: number, colIndex: number, ship: CheckShipType[]){
        if (rowIndex + 1 < 10
            && (field[rowIndex + 1][colIndex] === CellValueEnum.ship || field[rowIndex + 1][colIndex] === CellValueEnum.kill)) {
            ship.push({
                row: rowIndex + 1,
                col: colIndex,
                status: field[rowIndex + 1][colIndex]
            })
            field[rowIndex + 1][colIndex] = CellValueEnum.support
            this.checkColumn(field, rowIndex + 1, colIndex, ship)
        }
    }

    checkLivesShipsInField = (): CheckShipType[][] => {
        const copyField = this.field.map(row => row.map(cell => cell))

        const ships: CheckShipType[][] = []

        copyField.forEach((row, rowIndex) => {
            row.forEach((cell, colIndex) => {
                if (copyField[rowIndex][colIndex] === CellValueEnum.ship || copyField[rowIndex][colIndex] === CellValueEnum.kill) {
                    const ship: any[] = []
                    this.checkRow(row, rowIndex, colIndex, ship)
                    if (ship.length === 1) {
                        this.checkColumn(copyField, rowIndex, colIndex, ship)
                    }
                    ships.push(ship)
                }
            })
        })
        ships.sort((a,b) => b.length - a.length)
        return ships
    }

    checkCell = (row: number, col: number, coordinates: number[][] = []) => {
        const isNeibShipped = this.checkCellNeibValues(row, col, CellValueEnum.ship)
        if (isNeibShipped) {
            return false
        }

        const neibKilled = this.checkCellNeibValues(row, col, CellValueEnum.kill)
        if (neibKilled) {
            if (coordinates.some(cor => JSON.stringify(cor) === JSON.stringify([row, col]))) {
                coordinates.push([row, col])
            }
            neibKilled.forEach(el => {
                if (!coordinates.some(cor => JSON.stringify(cor) === JSON.stringify(el))) {
                    coordinates.push(el)
                    this.checkCell(el[0], el[1], coordinates)
                }
            })
            if (coordinates) {
                return coordinates
            }
        }
        const isNeibNotShip = this.checkCellNeibisNotValues(row, col, CellValueEnum.ship)

        const isNeibNotKill = this.checkCellNeibisNotValues(row, col, CellValueEnum.kill)

        if (isNeibNotShip && isNeibNotKill) {
            coordinates.push([row, col])
            return coordinates
        }
    }

    setMissValues (line: number[][]) {
        line.forEach(([row, col]) => {
            if (col - 1 >= 0 && this.field[row][col-1] === CellValueEnum.empty) {
                this.field[row][col-1] = CellValueEnum.miss
            }
            if (col - 1 >= 0 && row - 1 >= 0 && this.field[row - 1][col-1] === CellValueEnum.empty) {
                this.field[row-1][col-1] = CellValueEnum.miss
            }
            if (col - 1 >= 0 && row + 1 <= 9 && this.field[row + 1][col-1] === CellValueEnum.empty) {
                this.field[row+1][col-1] = CellValueEnum.miss
            }
            if (row + 1 <= 9 && this.field[row + 1][col] === CellValueEnum.empty) {
                this.field[row+1][col] = CellValueEnum.miss
            }
            if (row - 1 >= 0 && this.field[row - 1][col] === CellValueEnum.empty) {
                this.field[row-1][col] = CellValueEnum.miss
            }
            if (col + 1 <= 9 && this.field[row][col+1] === CellValueEnum.empty) {
                this.field[row][col+1] = CellValueEnum.miss
            }
            if (col + 1 <= 9 && row - 1 >= 0 && this.field[row - 1][col+1] === CellValueEnum.empty) {
                this.field[row-1][col+1] = CellValueEnum.miss
            }
            if (col + 1 <= 9 && row + 1 <= 9 && this.field[row + 1][col+1] === CellValueEnum.empty) {
                this.field[row+1][col+1] = CellValueEnum.miss
            }
        })
    }

    checkNeibCell = (row: number, col: number, value: CellValueEnum) => {
        if (this.check_D_R_forCell(row, col, value)) return true
        if (this.check_D_forCell(row, col, value)) return true
        if (this.check_D_L_forCell(row, col, value)) return true
        if (this.check_R_forCell(row, col, value)) return true
        if (this.check_L_forCell(row, col, value)) return true
        if (this.check_T_R_forCell(row, col, value)) return true
        if (this.check_T_forCell(row, col, value)) return true
        if (this.check_T_L_forCell(row, col, value)) return true
        return false
    }

    getNeibVariantsForKilledCell (row: number, col: number) {
        const resultArr = []
        if ((this.check_T_forCell(row, col, CellValueEnum.empty) || this.check_T_forCell(row, col, CellValueEnum.ship))
            && (this.checkNotCell(row, col+1, CellValueEnum.kill) && this.checkNotCell(row, col-1, CellValueEnum.kill))
        ) resultArr.push(`${row-1}-${col}`)
        if ((this.check_L_forCell(row, col, CellValueEnum.empty) || this.check_L_forCell(row, col, CellValueEnum.ship))
            && (this.checkNotCell(row-1, col, CellValueEnum.kill) && this.checkNotCell(row+1, col, CellValueEnum.kill))
        ) resultArr.push(`${row}-${col-1}`)
        if ((this.check_R_forCell(row, col, CellValueEnum.empty) || this.check_R_forCell(row, col, CellValueEnum.ship))
            && (this.checkNotCell(row-1, col, CellValueEnum.kill) && this.checkNotCell(row+1, col, CellValueEnum.kill))
        ) resultArr.push(`${row}-${col+1}`)
        if ((this.check_D_forCell(row, col, CellValueEnum.empty) || this.check_D_forCell(row, col, CellValueEnum.ship))
            && (this.checkNotCell(row, col+1, CellValueEnum.kill) && this.checkNotCell(row, col-1, CellValueEnum.kill))
        ) resultArr.push(`${row+1}-${col}`)
        return resultArr
    }

    isEqual (secondField: FieldType) {
        return JSON.stringify(this.field) === JSON.stringify(secondField)
    }

    getRandomCompletedField () {
        this.setField(this.getEmptyField())
        this.addRandomShipInField(4)
            .addRandomShipInField( 3)
            .addRandomShipInField( 3)
            .addRandomShipInField( 2)
            .addRandomShipInField( 2)
            .addRandomShipInField(2)
            .addRandomShipInField(1)
            .addRandomShipInField(1)
            .addRandomShipInField(1)
            .addRandomShipInField(1)
        this.ships = this.checkLivesShipsInField()
        return this
    }
}