type Coordinate = {
    x: number,
    y: number
}

export const prepareMouseCoordinates = (oldCoordinates: Coordinate, newCoordinates: Coordinate) => {
    const differenceX = (newCoordinates.x - oldCoordinates.x)
    const differenceY = (newCoordinates.y - oldCoordinates.y)
    if ((Math.abs(differenceX) + Math.abs(differenceY)) > 10) {
        const result = []

        if (differenceX === 0) {
            for (let i = 1; i < differenceY; i += 3) {
                result.push({
                    x: oldCoordinates.x,
                    y: oldCoordinates.y + i * (differenceY/Math.abs(differenceY))
                })
            }
        } else if (differenceY === 0) {
            for (let i = 1; i < differenceY; i += 3) {
                result.push({
                    x: oldCoordinates.x + i * (differenceX/Math.abs(differenceX)),
                    y: oldCoordinates.y
                })
            }
        } else {
            let ratioXtoY = differenceX / differenceY
            if (ratioXtoY < 0) {
                ratioXtoY *= -1
            }

            if (Math.abs(differenceX) > Math.abs(differenceY)) {
                for (let i = 1; i < Math.abs(differenceX); i += 3) {
                    result.push({
                        x: oldCoordinates.x + i * (differenceX/Math.abs(differenceX)),
                        y: oldCoordinates.y + i/ratioXtoY * (differenceY/Math.abs(differenceY))
                    })
                }
            } else {
                for (let i =1; i <= Math.abs(differenceY); i += 3) {
                    result.push({
                        x: oldCoordinates.x + i*ratioXtoY * (differenceX/Math.abs(differenceX)),
                        y: oldCoordinates.y + i * (differenceY/Math.abs(differenceY))
                    })
                }
            }

        }
        result.push(newCoordinates)
        return result
    }
    return []
}

export const onMoveSocket = async (cursorRef: any, res: any, yDefault: number = 19, xDefault: number = 19) => {
    if (cursorRef?.current) {
        const coords = prepareMouseCoordinates(
            // @ts-ignore
            {x: cursorRef.current.coordX || 0, y: cursorRef.current.coordY || 0 },
            {x: res.fieldX, y: res.fieldY})
        // @ts-ignore
        if (!cursorRef?.current?.stackRender) {
            // @ts-ignore
            cursorRef.current.stackRender = 0
            // console.log('------')

        } else {
            // let interval = setInterval(() => {
            //     console.log('interval')
            //     if (cursorRef.current.stackRender > 0) {
            //         cursorRef.current.stackRender -= 1
            //     } else {
            //         clearInterval(interval)
            //     }
            // }, 1)
        }
        if (coords.length) {
            coords.forEach((coord, i) => {
                setTimeout(() => {
                    // console.log('cicle')
                    // @ts-ignore
                    cursorRef.current.style.top = `${Math.round(coord.y + yDefault)}px`
                    // @ts-ignore
                    cursorRef.current.style.left = `${Math.round(coord.x + xDefault)}px`
                    // @ts-ignore
                    // if (i === coords.length - 1) {
                    //     // @ts-ignore
                    //     cursorRef.current.stackRender -= coords.length
                    // }
                    // cursorRef.current.stackRender -= 1
                    // console.log(cursorRef.current.stackRender)
                    // @ts-ignore
                }, 3)
            })
            cursorRef.current.stackRender += coords.length
            // // @ts-ignore
            // cursorRef.current.stackRender += coords.length
        } else {
            // setTimeout(() => {
                if (cursorRef?.current) {
                    // console.log(cursorRef.current.stackRender )
                    // @ts-ignore
                    cursorRef.current.style.top = `${res.fieldY + yDefault}px`
                    // @ts-ignore
                    cursorRef.current.style.left = `${res.fieldX + xDefault}px`
                    // // @ts-ignore
                    // cursorRef.current.stackRender = 0
                }
                cursorRef.current.stackRender +=1
                // // @ts-ignore
                // cursorRef.current.stackRender -=1
                // @ts-ignore
            // }, (cursorRef.current.stackRender + 1))
        }
        // @ts-ignore
        // console.log(cursorRef.current.stackRender)
        // @ts-ignore
        cursorRef.current.coordX = res.fieldX
        // @ts-ignore
        cursorRef.current.coordY = res.fieldY
    }
}