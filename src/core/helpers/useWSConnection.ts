import {useEffect} from "react";
import {io, Socket} from "socket.io-client";
import {AuthAppStateType} from "../types/authTypes";
import {FieldConstructor} from "./FieldConstructor";

type UseWSConnectionFuncType = (args: {
    auth: AuthAppStateType,
    notAuthName?: string | null
    setIoSocket: (s: Socket | null) => void,
    setGameId: (s: string | null) => void
    setEnemyField: (s: FieldConstructor) => void
    setEnemyId: (s: string | null) => void
    setEnemyData: (s: any) => void
    setIsLoadingEnemy: (s: boolean) => void
    setIsVisibleHeader: (s: boolean) => void
    setIsLoadingGame: (s: boolean) => void
    setLoadingMessage: (s: string) => void,
    setSelfField: (f: FieldConstructor) => void
    setGameResult: (f: any) => void
    onLeaveWaitingListCb: () => void
    setOnlinePlayersCount?: (count: number) => void
}) => void

export const useWSConnection: UseWSConnectionFuncType = (
    {
        auth,
        setIoSocket,
        setGameId,
        setEnemyField,
        setEnemyId,
        setEnemyData,
        setIsLoadingGame,
        setIsLoadingEnemy,
        setLoadingMessage,
        setSelfField,
        setGameResult,
        setIsVisibleHeader,
        onLeaveWaitingListCb,
        notAuthName,
        setOnlinePlayersCount,
    }
) => {
    useEffect(() => {
            if (auth.userId || notAuthName) {
                const socket = io(process.env.NODE_ENV === "production" ? 'https://sea-battles.ru' : 'http://localhost:4400', {
                    withCredentials: true,
                    extraHeaders: {
                        userId: auth.userId || notAuthName as string
                    }
                });

                setIoSocket(socket)

                socket.on("connect", () => {});

                socket.on('startGame', (res) => {
                    setGameId(res.gameId)
                    // @ts-ignore
                    setEnemyField(new FieldConstructor(res.enemyField))
                    setEnemyId(res.enemyId)
                    setEnemyData(res.enemyData)

                    if (res.isYouStep) {
                        setIsLoadingEnemy(false)
                        setIsLoadingGame(false)
                    } else {
                        setIsLoadingEnemy(true)
                        setLoadingMessage('Ход противника')
                        setIsLoadingGame(false)
                    }
                })

                socket.on('leaveWaitingList', () => {
                    onLeaveWaitingListCb()
                })

                socket.on('strike', (res) => {
                    if (res.isYouStep) {
                        setIsLoadingEnemy(false)
                        setEnemyField(new FieldConstructor(res.enemyField))
                        setSelfField(new FieldConstructor(res.playerField))
                    } else {
                        setIsLoadingEnemy(true)
                        setEnemyField(new FieldConstructor(res.enemyField))
                        setSelfField(new FieldConstructor(res.playerField))
                    }
                })

                socket.on('endGame', (res) => {
                        setIsLoadingEnemy(true)
                        setEnemyField(new FieldConstructor(res.enemyField))
                        setSelfField(new FieldConstructor(res.playerField))
                        setGameResult(res.result)
                        setIsVisibleHeader(true)
                        socket.emit('playersCount')
                })

                socket.on('playersCount', (res) => {
                    setOnlinePlayersCount?.(res)
                })

                socket.emit('playersCount')

                return () => {
                    socket.close()
                };
            }
        }, [auth, notAuthName]
    )
}
