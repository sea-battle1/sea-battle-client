export const defaultAuthState = {
    isAuth:false,
    first_name: null,
    authType: null,
    last_name: null,
    userId: null
}
