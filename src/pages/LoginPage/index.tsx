import React, {useEffect} from "react";
import styles from './styles.module.css'
import {AuthAppStateType, AuthTypes, VkAuthResponseType, VKConnectionStatus} from "../../core/types/authTypes";
import {api} from "../../core/api";
import {useNavigate} from "react-router-dom";
import {getVKAPI, loginGoogle} from "../../components/AppContainer/hooks";

type AuthContainerPropTypes = {
    setAuthUser: (auth: AuthAppStateType) => void
    setIsLoading: (isLoading: boolean) => void
    isLoadedGoogleApi: boolean,
    setIsLoadedGoogleApi: (v: boolean) => void
    isLoadedVKApi: boolean,
    setIsLoadedVKApi: (v: boolean) => void
}

const LoginPage: React.FC<AuthContainerPropTypes> = ({setAuthUser, setIsLoading, isLoadedVKApi, isLoadedGoogleApi, setIsLoadedVKApi, setIsLoadedGoogleApi}) => {
    const navigate = useNavigate();

    const onClickVKAuth = async () => {
        setIsLoading(true)
        // @ts-ignore
        await window.VK.Auth.login(async (data: VkAuthResponseType) => {
            if (data.status === VKConnectionStatus.connected) {
                const res = await api.login({...data.session, authType: AuthTypes.VK})
                setAuthUser({
                    isAuth: true,
                    userId: res.userId,
                    first_name: res.first_name,
                    last_name: res.last_name,
                    authType: AuthTypes.VK
                })
                localStorage.setItem('tokenBearer', res.bearer)
            }
            setIsLoading(false)
        })
        navigate('/')
    }

    useEffect(() => {
        (async function () {
            if (!isLoadedVKApi) {
                await getVKAPI()
            }
            if (!isLoadedGoogleApi) {
                await loginGoogle(setAuthUser)
            }
            // @ts-ignore
            if (window.google?.accounts?.id) {
                // @ts-ignore
                window.google.accounts.id.renderButton(document.getElementById('google'), {
                    size: "large",
                    theme: 'filled_black'
                });
            } else {
                setTimeout(() => {
                    // @ts-ignore
                    window.google.accounts.id.renderButton(document.getElementById('google'), {
                        size: "large",
                        theme: 'filled_black'
                    });
                }, 1500)
            }
        })()

    }, [isLoadedVKApi, isLoadedGoogleApi])

    return <div className={styles.authContainer}>
        <div className={styles.authBlock}>
            <h1 className={styles.authTitle}>Авторизация</h1>
            <div id="google"></div>

            <div className={styles.additionalBtns}>
                Авторизоваться через ВК
                <button onClick={onClickVKAuth} className={styles.vkLogo}/>
            </div>
        </div>
    </div>
}

export default LoginPage