import React, {useEffect, useState} from "react";
import {Table, Tag} from 'antd';
import type {ColumnsType} from 'antd/es/table';
import {api} from "../../core/api";
import styles from './styles.module.css'
import {AuthTypes} from "../../core/types/authTypes";

const columns: ColumnsType<any> = [
    {
        title: '',
        dataIndex: 'authType',
        width: 30,
        render: (value: string) => {
            if (value.toUpperCase() === AuthTypes.VK) {
                return <img src={require('./../../core/assets/images/vkLogoPng.png')} width={20} height={20} alt={'npne'} />
            }
            if (value.toUpperCase() === AuthTypes.Google.toUpperCase()) {
                return <img src={require('./../../core/assets/images/googleLogo.png')} width={20} height={20} alt={'npne'} />

            }
            if (value.toUpperCase() === AuthTypes.TG.toUpperCase()) {
                return <img src={require('./../../core/assets/images/tgLogo.png')} width={20} height={20} alt={'npne'} />

            }
            return null
        }
    },
    {
        title: 'Ник',
        dataIndex: 'nick_name',
        key: 'nick_name',
    },
    {
        title: 'Имя',
        dataIndex: 'first_name',
    },
    {
        title: 'Фамилия',
        dataIndex: 'last_name',
    },
    {
        title: 'Сыграно',
        dataIndex: 'games',
        sorter: (a, b) => a.games - b.games,
    },
    {
        title: 'Победы',
        dataIndex: 'wins',
        sorter: (a, b) => a.wins - b.wins,
    },
    {
        title: 'Процент побед',
        dataIndex: 'relative',
        sorter: (a, b) => +a.relative - +b.relative,
    },
    {
        title: 'Дата регистрации',
        dataIndex: 'createdAt',
        // @ts-ignore
        sorter: (a, b) => (new Date(a.createdAt)).getTime()- (new Date(b.createdAt)).getTime(),
        render: (value: string) => <span>{(new Date(value)).toLocaleString()}</span>
    }
];


const UsersListPage = () => {
    const [usersList, setUsersList] = useState<any[] | null>(null)
    const [isLoading, setIsLoading] = useState<boolean>(true)

    useEffect(() => {
        (async function () {
            try {
                const users = await api.getUsersList()
                if (users) {
                    setUsersList(users.map(u => ({
                        ...u,
                        games: u.allGames,
                        wins: u.allWin,
                        relative: u.relativePercent,
                        authType: u.userId.split('_')[0]
                    })))
                }
                setIsLoading(false)
            } catch (err) {
                console.error(err)
                setIsLoading(false)
            }
        })()
    }, [])

    return <div>
        <Table
            className={styles.table}
            columns={columns}
            loading={isLoading}
            dataSource={usersList || []}
            bordered
            rowKey="userId"
            scroll={{x: true}}
            size="small"
            pagination={{
                pageSize: 24,
                showTotal: () => <Tag color={"blue-inverse"}>{usersList?.length} игроков</Tag>,
            }}
        />

    </div>
}

export default UsersListPage