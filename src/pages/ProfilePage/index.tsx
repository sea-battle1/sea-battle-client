import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import * as Sentry from "@sentry/react";
import {message} from 'antd'
import {api} from "../../core/api";
import {UserType} from "../../core/types/userTypes";
import {RecordReplayFromList} from "../../core/types/fieldTypes";
import {ReplaysList} from "./components/ReplaysList";

const ProfilePage = () => {
    const [userInfo, setUserInfo] = useState<UserType | null>(null)
    const [gameRecords, setGameRecords] = useState<RecordReplayFromList[] | null>(null)

    let params = useParams();
    useEffect(() => {
        (async function () {
            try {
                if (params.userId) {
                    const userInfoRes = await api.getUserInfo(params.userId)
                    if (userInfoRes) {
                        setUserInfo(userInfoRes)
                    }
                    const records = await api.getReplayRecordsByUserId(params.userId)
                    if (records?.records) {
                        setGameRecords(records.records)
                    }
                }
            } catch (err) {
                Sentry.captureException(err);
                message.open({
                    type: 'error',
                    content: 'Не удалось загрузить данные пользователя'
                })
                console.error(err)
            }
        })()
    }, [])

    if (!userInfo) {
        return <span>Пользователь с таким id не найден</span>
    }

    return <div>
        <h1>{userInfo.first_name} {userInfo.last_name}</h1>
        <h3>Одиночная игра</h3>
        <p>Начато игр: {userInfo.singleGames}</p>
        <p>Выиграно игр: {userInfo.singleWin}</p>
        <p>Проиграно игр: {userInfo.singleLoose}</p>
        <h3>Игра ботов</h3>
        <p>Сделано ставок: {userInfo.simulateGames}</p>
        <p>Выиграно: {userInfo.simulateWin}</p>
        <p>Проиграно: {userInfo.simulateLoose}</p>
        <h3>Многопользовательская игра</h3>
        <p>Начато игр: {userInfo.mpGames}</p>
        <p>Выиграно: {userInfo.mpWin}</p>
        <p>Проиграно: {userInfo.mpLoose}</p>
        {gameRecords && <ReplaysList replays={gameRecords} profileId={params.userId as string}/>}
    </div>
}

export default ProfilePage