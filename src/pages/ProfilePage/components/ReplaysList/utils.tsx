import React from "react";
import {GameRecordTypes, RecordReplayFromList} from "../../../../core/types/fieldTypes";
import {ColumnsType} from "antd/es/table";
import {Button, Tag} from "antd";
import {NavLink} from "react-router-dom";
import styles from './styles.module.css'

export const getResultRecord = (isTimeOut: boolean, userIsWinner: boolean): string => {
    if (isTimeOut && userIsWinner) return 'Победа по таймауту'
    if (isTimeOut && !userIsWinner) return 'Поражение по таймауту'
    if (!isTimeOut && !userIsWinner) return 'Поражение'
    return 'Победа'
}

export const getRecordType = (recordType:GameRecordTypes ): string => {
    if (recordType === GameRecordTypes.mp) return 'Многопользовательская'
    return 'Одиночная'
}

export const columns: ColumnsType<RecordReplayFromList> = [
    {
        title: 'Дата',
        dataIndex: 'gameStartTime',
        key: 'gameStartTime',
        sorter: (a, b) => a.gameStartTime - b.gameStartTime,
        render: (value) => <span>{(new Date(value)).toLocaleString()}</span>
    },
    {
        title: '',
        key: 'gameId',
        dataIndex: 'gameId',
        width: 132,
        render: (gameId) => <NavLink to={`/replayGame/${gameId}`}><Button className={styles.playRecordBtn} type="primary">Воспроизвести</Button></NavLink>
    },
    {
        title: 'Тип игры',
        dataIndex: 'type',
        key: 'type',
        render: (value) => <Tag
            color={value === GameRecordTypes.mp ? 'blue' : 'yellow'}
        >{value === GameRecordTypes.mp ? 'Многопользовательская' : 'Одиночная'}</Tag>
    },
    {
        title: 'Результат',
        dataIndex: 'result',
        key: 'result',
        render: (value, otherValues) => <Tag
            color={otherValues.userIsWinner ? 'green' : 'red'}
        >{value}</Tag>
    },
    {
        title: 'Время игры',
        dataIndex: 'durationGame',
        key: 'durationGame',
        render: (durationGame) => <span>
            {durationGame.toFixed(1)} сек.
        </span>
    },
    {
        title: 'Ваши ходы',
        dataIndex: 'userSteps',
        key: 'userSteps',
    },
    {
        title: 'Ходы противника',
        dataIndex: 'enemySteps',
        key: 'enemySteps',
    },
];
