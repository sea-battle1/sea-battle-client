import React from "react";
import {Table} from "antd";
import {RecordReplayFromList} from "../../../../core/types/fieldTypes";
import styles from './styles.module.css'
import {columns} from "./utils";

type ReplaysListProps = {
    replays: RecordReplayFromList[],
    profileId: string
}

export const ReplaysList: React.FC<ReplaysListProps> = ({replays, profileId}) => {
    return <div className={styles.replaysContainer}>
        <h3 className={styles.tableTitle}>Ваши бои: </h3>
        <Table
            scroll={{x: true}}
            bordered
            className={styles.tableReplays}
            columns={columns}
            dataSource={replays}
            rowKey="gameId"
        />
    </div>
}