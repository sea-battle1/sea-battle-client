import React from "react";
import {SingleGameContainer} from "./components/SingleGameContainer";
import {AuthAppStateType} from "../../core/types/authTypes";

type SingleGamePageProps = {
    setIsVisibleHeader: (value:boolean) => void,
    auth: AuthAppStateType
}

const SingleGamePage: React.FC<SingleGamePageProps> = ({setIsVisibleHeader, auth}) => {
    return <SingleGameContainer setIsVisibleHeader={setIsVisibleHeader} auth={auth} />
}

export default SingleGamePage