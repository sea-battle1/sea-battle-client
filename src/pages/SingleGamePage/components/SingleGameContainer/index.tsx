import React, {useEffect, useState} from "react";
import clsx from "clsx";
import {Button} from "antd";
import {GameResultEnum} from "../../../../core/constants/enums";
import {Timer} from "../../../../components/Timer";
import {AppConfig} from "../../../../appConfig";
import {api} from "../../../../core/api";
import {FieldConstructor} from "../../../../core/helpers/FieldConstructor";
import {AuthAppStateType} from "../../../../core/types/authTypes";
import {PlayCircleOutlined} from "@ant-design/icons";
import {FieldsGameBlock} from "../../../../components/FieldsGameBlock";
import {ResultModal} from "../../../../components/ResultModal";
import {TodayGameCount} from "../../../../components/TodayGameCount";
import styles from './styles.module.css'
import appStyles from './../../../../components/AppContainer/styles.module.css'
import {AxiosError} from "axios";
import {OnlinePlayersCount} from "../../../../components/OnlinePlayersCount";

type SingleGameContainerProps = {
    setIsVisibleHeader: (value: boolean) => void,
    auth: AuthAppStateType
}

const gameConfig: any = {
    startTime: null,
    yourStep: 0,
    enemyStem: 0
}

export const SingleGameContainer: React.FC<SingleGameContainerProps> = ({setIsVisibleHeader, auth}) => {
    const [leftField, setLeftField] = useState<FieldConstructor>(new FieldConstructor().getRandomCompletedField())
    const [rightField, setRightField] = useState<FieldConstructor>(new FieldConstructor())
    const [gameId, setGameId] = useState<string | null>(null)
    const [isLoadingBotField, setIsLoadingBotField] = useState<boolean>(false)
    const [gameResult, setGameResult] = useState<GameResultEnum>(GameResultEnum.isNonWin)
    const [gamesCountToday, setGameCountToday] = useState<number | null>(null);
    const [isLoadingCount, setIsLoadingCount] = useState<boolean>(true)

    const updateGamesCountToday = async () => {
        setIsLoadingCount(true)
        try {
            const recordCountToday = await api.getRecordsCountByDay(Date.now())

            setGameCountToday(recordCountToday)
            // @ts-ignore
        } catch (err: AxiosError) {
            if (err.response.status === 406) {
                setGameCountToday(0)
            }
        }
        setIsLoadingCount(false)
    }

    const onStartSingleGame = async () => {
        const res = await api.startSingleGame(leftField.field)
        setGameId(res.gameId)
        setRightField(new FieldConstructor(res.enemyField))
        setIsVisibleHeader(false)
        gameConfig.startTime = Date.now()
    }

    const onRestart = () => {
        setGameResult(GameResultEnum.isNonWin)
        setGameId(null)
        gameConfig.startTime = null
        gameConfig.yourStep = 0
        gameConfig.enemyStem = 0
        setIsLoadingBotField(false)
        setLeftField(new FieldConstructor().getRandomCompletedField())
        updateGamesCountToday()
    }

    const onClickEmptyCell = async (rowIndex: number, columnIndex: number, isRecurse = false) => {
        try {
            setIsLoadingBotField(true)
            const res = await api.strikeCell(gameId as string, rowIndex, columnIndex)
            gameConfig.yourStep += 1
            gameConfig.enemyStem += 1
            if (res.isEndGameOfTimeOut) {
                setGameResult(GameResultEnum.isBotWin)
                setGameId(null)
            } else {
                if (isRecurse) {
                    gameConfig.enemyStem -= 1
                }
                setRightField(new FieldConstructor(res.botField))
                setLeftField(new FieldConstructor(res.playerField))
                if (res.isKillStrike) {
                    setTimeout(() => {
                        onClickEmptyCell(rowIndex, columnIndex, true)
                    }, 500)
                } else if (res.isPlayerWin) {
                    setGameResult(GameResultEnum.isPlayerWin)
                    setIsVisibleHeader(true)
                } else if (res.isBotWin) {
                    setGameResult(GameResultEnum.isBotWin)
                    setIsVisibleHeader(true)
                } else {
                    setIsLoadingBotField(false)
                }
            }
        } catch (err) {
            setIsLoadingBotField(false)
        }

    }

    const onEndTime = () => {
        setIsVisibleHeader(true)
        setGameResult(GameResultEnum.isBotWin)
    }

    useEffect(() => {(async function () {await updateGamesCountToday()})()}, [])

    return <div className={clsx(appStyles.appContainer)}>
        {!gameId && <div className={styles.nowStatsTags}>
             <TodayGameCount isLoadingCount={isLoadingCount} count={gamesCountToday || 0}/>
        </div>}
        <FieldsGameBlock
            onClickEmptyCell={onClickEmptyCell}
            isLoadingBotField={isLoadingBotField}
            setLeftField={setLeftField}
            rightField={rightField}
            leftField={leftField}
            gameIsPlaying={!!gameId}
        />
        <div className={styles.btnContainer}>
            {!gameId && <Button className={styles.startBtn} type="primary" onClick={onStartSingleGame}>Старт <PlayCircleOutlined /></Button>}
            {gameResult !== GameResultEnum.isNonWin &&
                <Button type="primary" onClick={onRestart}>Начать заного</Button>}
        </div>
        {gameId && (gameResult === GameResultEnum.isNonWin) && <Timer
            restartProp={rightField}
            startTime={AppConfig.singleGame.interval / 1000}
            onEndTime={onEndTime}/>
        }
        <ResultModal
            isOpen={gameResult !== GameResultEnum.isNonWin}
            gameResult={gameResult}
            leftField={leftField}
            rightField={rightField}
            onOk={onRestart}
            gameConfig={gameConfig}
            gameId={gameId}
            userId={auth.userId}
        />
    </div>
}
