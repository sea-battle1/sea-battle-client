import React, {useEffect, useRef, useState} from "react";
import {Button, Input, Spin} from "antd";
import {CheckCircleOutlined, PlayCircleOutlined} from "@ant-design/icons";
import {Socket} from "socket.io-client";
import {AxiosError} from "axios";
import {GameResultEnum} from "../../core/constants/enums";
import {AuthAppStateType} from "../../core/types/authTypes";
import {onMoveSocket} from "../../core/helpers/prepareMouseCoordinates";
import {Timer} from "../../components/Timer";
import {AppConfig} from "../../appConfig";
import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {FieldsGameBlock} from "../../components/FieldsGameBlock";
import {ResultModal} from "../../components/ResultModal";
import {EnemyInfoBlock} from "../../components/EnemyInfoBlock";
import {useWSConnection} from "../../core/helpers/useWSConnection";
import styles from "./styles.module.css";
import appStyles from './../../components/AppContainer/styles.module.css'
import {OnlinePlayersCount} from "../../components/OnlinePlayersCount";
import {api} from "../../core/api";
import {TodayGameCount} from "../../components/TodayGameCount";

type  MultiplayerGamePageProps = {
    auth: AuthAppStateType
    setIsVisibleHeader: (v: boolean) => void
}

const MultiplayerGame: React.FC<MultiplayerGamePageProps> = ({auth, setIsVisibleHeader}) => {
    const [selfField, setSelfField] = useState<FieldConstructor>(new FieldConstructor().getRandomCompletedField())
    const [enemyField, setEnemyField] = useState<FieldConstructor>(new FieldConstructor())
    const [gameId, setGameId] = useState<string | null>(null)
    const [enemyId, setEnemyId] = useState<string | null>(null)
    const [isLoadingGame, setIsLoadingGame] = useState<boolean>(false)
    const [isLoadingEnemy, setIsLoadingEnemy] = useState<boolean>(false);
    const [loadingMessage, setLoadingMessage] = useState<string>('')
    const [gameResult, setGameResult] = useState<GameResultEnum>(GameResultEnum.isNonWin)
    const [inputValue, setInputValue] = useState<string>('')
    const [notAuthName, setNotAuthName] = useState<string>('')
    const [enemyData, setEnemyData] = useState<string | null>(null)
    const [ioSocket, setIoSocket] = useState<Socket | null>(null)
    const [onlinePlayersCount, setOnlinePlayersCount] = useState<number>(1);
    const [isLoadingCount, setIsLoadingCount] = useState<boolean>(true)
    const [gamesCountToday, setGameCountToday] = useState<number | null>(null);

    const cursorRef = useRef<HTMLDivElement>(null)

    const updateGamesCountToday = async () => {
        setIsLoadingCount(true)
        try {
            const recordCountToday = await api.getRecordsCountByDay(Date.now())

            setGameCountToday(recordCountToday)
            // @ts-ignore
        } catch (err: AxiosError) {
            if (err.response.status === 406) {
                setGameCountToday(0)
            }
        }
        setIsLoadingCount(false)
    }

    const onRestart = () => {
        setGameResult(GameResultEnum.isNonWin)
        setGameId(null)
        setEnemyData(null)
        setIsLoadingGame(false)
        setSelfField(new FieldConstructor().getRandomCompletedField())
        updateGamesCountToday()
    }

    const onStart = async () => {
        setIsLoadingGame(true)
        try {
            setLoadingMessage('Идет поиск игроков')
            ioSocket?.emit('startGame', {
                type: 'startGame',
                userId: auth.userId || notAuthName,
                userField: selfField.field
            });
            setIsVisibleHeader(false)
        } catch (err) {
            console.error(err)
        }
    }

    const onClickEnemySell = (rowIndex: number, columnIndex: number) => {
        setIsLoadingEnemy(true)
        ioSocket?.emit('strike', {
            type: 'strike',
            gameId,
            enemyId,
            userId: auth.userId || notAuthName,
            rowIndex,
            columnIndex,
        })
    }

    const onMoveEnemyField = (fieldY: number, fieldX: number, type: string) => {
        // wSocket?.send(JSON.stringify({
        //     type: type === 'mousemove' ? 'move' : type,
        //     gameId,
        //     enemyId,
        //     userId: auth.userId || notAuthName,
        //     fieldY,
        //     fieldX,
        // }));
    }

    const leaveWaitingListClick = () => {
        ioSocket?.emit('leaveWaitingList', true)
    }

    const onLeaveWaitingListCb = () => {
        setIsLoadingGame(false)
        setLoadingMessage('')
    }

    useWSConnection({
        auth,
        notAuthName,
        setIoSocket,
        setGameId,
        setLoadingMessage,
        setIsLoadingEnemy,
        setIsLoadingGame,
        setEnemyData,
        setEnemyId,
        setEnemyField,
        setSelfField,
        setGameResult,
        setIsVisibleHeader,
        onLeaveWaitingListCb,
        setOnlinePlayersCount
    })

    useEffect(() => { updateGamesCountToday() }, [])

    return <>
        <Spin size="large" className={styles.spinner} tip={loadingMessage} spinning={isLoadingGame}>
        <div className={appStyles.appContainer}>
            {!gameId && <div className={styles.nowStatsTags}>
                <TodayGameCount isLoadingCount={isLoadingCount} count={gamesCountToday || 0}/>
                <OnlinePlayersCount count={onlinePlayersCount}/>
            </div>}
            {!auth.isAuth && !notAuthName && <div
                className={styles.inputBtnCont}
            >
                <Input status={'error'} value={inputValue} onChange={(e) => setInputValue(e.target.value)}
                       className={styles.nameInput} bordered placeholder="Введите ник"
                />
                <Button
                    onClick={() => setNotAuthName(inputValue)}
                    type="primary"
                >
                    <CheckCircleOutlined/>
                </Button>
            </div>}
            <FieldsGameBlock
                onClickEmptyCell={onClickEnemySell}
                isLoadingBotField={isLoadingEnemy}
                setLeftField={setSelfField}
                rightField={enemyField}
                leftField={selfField}
                gameIsPlaying={!!gameId}
                cursorRef={cursorRef}
                onMoveEnemyField={onMoveEnemyField}
            />
            <div className={styles.btnContainer}>
                {!gameId && (auth.isAuth || notAuthName) && <Button className={styles.startBtn} type="primary" onClick={onStart}>Старт <PlayCircleOutlined /></Button>}
            </div>
            {gameId && (gameResult === GameResultEnum.isNonWin) && !isLoadingEnemy && <Timer
                restartProp={selfField}
                startTime={AppConfig.singleGame.interval / 1000}
            />}
            {gameId && <EnemyInfoBlock enemyData={enemyData}/>}
            <ResultModal
                isOpen={gameResult !== GameResultEnum.isNonWin}
                gameResult={gameResult}
                leftField={selfField}
                rightField={enemyField}
                onOk={onRestart}
                gameId={gameId}
                userId={auth.userId}
            />
        </div>
    </Spin>
        {isLoadingGame && <Button onClick={leaveWaitingListClick}>Покинуть очередь</Button>}
    </>
}

export default MultiplayerGame
