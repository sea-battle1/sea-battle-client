import React from 'react'
import bridge from "@vkontakte/vk-bridge";
import {api} from "../../core/api";
import {AuthTypes} from "../../core/types/authTypes";
import {TourProps} from "antd";

export const options = [
    {
        label: 'Ваши друзья',
        value: 'vkFriends'
    },
    {
        label: 'Топ ВК',
        value: 'topVK'
    },
    {
        label: 'Топ всех игроков',
        value: 'topAll'
    },
]

export const getFriendsResults = async (vkAuthUserId: number, bearer?: string | null) => {
    try {
        const userToken = await bridge.send('VKWebAppGetAuthToken', {
            app_id: process.env.NODE_ENV === "production" ? 51517697 : 51534854,
            scope: 'friends, notifications'
        })

        const users = await bridge.send('VKWebAppCallAPIMethod', {
            method: 'execute',
            params: {
                access_token: userToken.access_token,
                v: '5.131',
                code: `var friendsList = API.friends.getAppUsers(); 
                               friendsList.unshift(${vkAuthUserId});
                               var friendListWithPhoto = API.users.get({
                                    user_ids: friendsList,
                                    fields: 'photo_100',
                                });
                                return {"friendListWithPhoto": friendListWithPhoto};`
            }
        })

        const profiles = await api.getUsersList({userIds: users.response.friendListWithPhoto.map((u: any) => `vk_${u.id}`).join(',')}, bearer)

        return profiles.filter(u => !!u).map((u: any) => {
            return {
                user: {
                    id: u.userId,
                    is_bot: false,
                    first_name: u.userId !== `vk_${vkAuthUserId}` ? u.first_name : '',
                    last_name: u.userId !== `vk_${vkAuthUserId}` ? u.last_name: 'Вы',
                    username: u.nick_name,
                    photoUrl: users.response.friendListWithPhoto.find((el: any) => el.id === +u.userId.split('_')[1])?.photo_100,
                },
                score: u.allWin,
                games: u.allGames
            }
        }).sort((a, b) => b.score - a.score)
    } catch (err) {
        return null
    }

}

export const getVkTopResults = async (bearer?: string | null) => {
    try {
        const usersFromDB = await api.getUsersList({
            authType: AuthTypes.VK,
            limit: 20,
            sort: true
        }, bearer)
        const userToken = await bridge.send('VKWebAppGetAuthToken', {
            app_id: process.env.NODE_ENV === "production" ? 51517697 : 51534854,
            scope: ''
        })

        const ids = usersFromDB.map(u => +u.userId?.split('_')[1]).join(',')
        const users = await bridge.send('VKWebAppCallAPIMethod', {
            method: 'execute',
            params: {
                access_token: userToken.access_token,
                v: '5.131',
                code: `var usersListWithPhoto = API.users.get({
                                    user_ids: [${ids}],
                                    fields: 'photo_100',
                                });
                                return {"usersListWithPhoto": usersListWithPhoto};`
            }
        })
        return users.response.usersListWithPhoto.map((u: any) => {
            const currentUserInDB: any = usersFromDB.find(uDB => uDB.userId === `vk_${u.id}`)
            return {
                user: {
                    id: currentUserInDB.userId,
                    is_bot: false,
                    first_name: currentUserInDB.first_name,
                    last_name: currentUserInDB.last_name,
                    username: currentUserInDB.nick_name,
                    photoUrl: u.photo_100,
                },
                score: currentUserInDB.singleWin + currentUserInDB.mpWin + currentUserInDB.simulateWin
            }
        }).sort((a: any, b: any) => b.score - a.score)
    } catch (err) {
        return null
    }
}

export const getAllTopResults = async (bearer?: string | null) => {
    try {
        const usersFromDB = await api.getUsersList({
            limit: 20,
            sort: true
        }, bearer)
        const vkUsers = usersFromDB.filter(u => u.authType === AuthTypes.VK).map(u => +u.userId?.split('_')[1]).join(',')

        const users = await bridge.send('VKWebAppCallAPIMethod', {
            method: 'execute',
            params: {
                access_token: process.env.NODE_ENV === "production"
                    ? 'a16b36d4a16b36d4a16b36d4fda2792fd5aa16ba16b36d4c2d8c9aef5e8949fb2b0af57'
                    : '1d2fb47b1d2fb47b1d2fb47b071e3de87d11d2f1d2fb47b7efed0af04eb85f1cc998b08',
                v: '5.131',
                code: `var usersListWithPhoto = API.users.get({
                                    user_ids: [${vkUsers}],
                                    fields: 'photo_100',
                                });
                                return {"usersListWithPhoto": usersListWithPhoto};`
            }
        })

        return usersFromDB.map(u => {
            const currentUser = users.response.usersListWithPhoto.find((el: any) => `vk_${el.id}` === u.userId)
            if (currentUser) {
                return {...u, photoUrl: currentUser.photo_100}
            }
            return u
        }).map((u) => {
            return {
                user: {
                    authType: u.authType,
                    id: u.userId,
                    is_bot: false,
                    first_name: u.first_name,
                    last_name: u.last_name,
                    username: u.nick_name,
                    // @ts-ignore
                    photoUrl: u.photoUrl
                },
                score: u.singleWin + u.mpWin + u.simulateWin
            }
        }).sort((a: any, b: any) => b.score - a.score)
    } catch (err) {
        return null
    }
}

export const getCurrentResults = (selectedFragment: string, friendsResults: any, vkTopResults: any, allTopResults: any) => {
    if (selectedFragment === 'vkFriends') return friendsResults
    if (selectedFragment === 'topVK') return vkTopResults
    if (selectedFragment === 'topAll') return allTopResults
}

export const getSteps = (): TourProps['steps']  => {
    return [
        {
            title: 'Обучение',
            description: 'Хотите пройти небольшое обучение для комфортной игры?',
            nextButtonProps: {
                children: 'Да'
            }
        },
        {
            title: 'Режимы игры',
            description: 'В игре доступны 2 режима игры. Многопользовательская игра и игра против компьютера. При старте игры вы встанете в очередь поиска противника. Если вы не хотите ждать или хотите играть против компьютера просто нажмите на кнопку играть против компьютера.',
            nextButtonProps: {
                children: 'Далее',
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Информация о ваших кораблях',
            description: 'В этом блоке отображаются корабли с вашего поля, убитые корабли будут закрашены',
            target: () => document.getElementById('FieldHeader__Player') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Игровое поле',
            description: 'В данном игровом поле располагаются ваши корабли',
            target: () => document.getElementById('Field__Self') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Автоматическая расстановка кораблей',
            placement: 'top',
            description: 'Нажмите сюда и корабли будут расставлены автоматически',
            target: () => document.getElementById('RandomShipSetterButton') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Ручная расстановка кораблей',
            placement: 'top',
            description: 'Если хотите установить корабли вручную, нажмите сюда, далее просто переносите корабли на поле',
            target: () => document.getElementById('ManualShipSetterButton') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Результаты других игроков',
            placement: 'top',
            description: 'В данном блоке отображаются результаты других игроков. Вы можете посмотреть результаты своих друзей, лучших игроков в вк и на других платформах',
            target: () => document.getElementById('ResultsBlocks') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Добавить игру в избранное',
            placement: 'top',
            description: 'Вы можете добавить игру в избранное для более удобного запуска',
            target: () => document.getElementById('FavoriteGameButton__VKGames') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Поделитесь игрой с друзьями',
            placement: 'top',
            description: 'Нажмите что бы поделиться игрой с друзьями',
            target: () => document.getElementById('ShareGameButton__VKGames') as HTMLElement,
            nextButtonProps: {
                children: 'Далее'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
        {
            title: 'Старт игры',
            placement: 'top',
            description: 'Для начала игры нажмите кнопку Старт. Хотите начать?',
            target: () => document.getElementById('StartGameButton__VKGames') as HTMLElement,
            nextButtonProps: {
                children: 'Завершить'
            },
            prevButtonProps: {
                children: 'Назад'
            }
        },
    ];
}