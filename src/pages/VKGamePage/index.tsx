import React, {useCallback, useEffect, useRef, useState} from "react";
import * as Sentry from "@sentry/react";
import bridge from "@vkontakte/vk-bridge";
import clsx from "clsx";
import {Button, message, Tour} from "antd";
import {PlayCircleOutlined, ShareAltOutlined, StarOutlined, UserOutlined, InfoCircleOutlined} from '@ant-design/icons';
import {GameResultEnum} from "../../core/constants/enums";
import {Timer} from "../../components/Timer";
import {AppConfig} from "../../appConfig";
import {api} from "../../core/api";
import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {AuthAppStateType, AuthTypes} from "../../core/types/authTypes";
import {ResultModal} from "../../components/ResultModal";
import {FindGameModal} from "../../components/FindGameModal";
import {FieldsGameBlock} from "../../components/FieldsGameBlock";
import {onMoveSocket} from "../../core/helpers/prepareMouseCoordinates";
import {ChatResults} from "../TGGamePage/components/ChatResults";
import {setIsAppLoading} from "../../core/helpers/setIsAppLoading";
import {getAllTopResults, getCurrentResults, getFriendsResults, getSteps, getVkTopResults, options} from "./utils";
import {SegmentedValue} from "antd/lib/segmented";
import styles from './styles.module.css'
import {EnemyInfoBlock} from "../../components/EnemyInfoBlock";
import {TodayGameCount} from "../../components/TodayGameCount";
import {useWSConnection} from "../../core/helpers/useWSConnection";
import {Socket} from "socket.io-client";
import {OnlinePlayersCount} from "../../components/OnlinePlayersCount";
import {FieldType} from "../../core/types/fieldTypes";
import {AxiosError} from "axios";
import {ProfileModal} from "../../components/ProfileModal";
import {InfoModal} from "../../components/InfoModal";

type VKGamePageProps = {
    auth: AuthAppStateType
}

let selfField: FieldType

const VKGamePage: React.FC<VKGamePageProps> = ({auth}) => {
    const [leftField, setLeftField] = useState<FieldConstructor>(() => {
        const firstRandomField = new FieldConstructor().getRandomCompletedField()
        if (!selfField) {
            selfField = firstRandomField.field
        }
        return firstRandomField
    })
    const [rightField, setRightField] = useState<FieldConstructor>(new FieldConstructor())
    const [gameId, setGameId] = useState<string | null>(null)
    const [isLoadingEnemyField, setIsLoadingEnemyField] = useState<boolean>(false)
    const [gameResult, setGameResult] = useState<GameResultEnum>(GameResultEnum.isNonWin)
    const [isOpenFindGameModal, setIsOpenFindGameModal] = useState<boolean>(false)
    const [enemyId, setEnemyId] = useState<string | null>(null)
    const [enemyData, setEnemyData] = useState<any | null>(null)
    const [friendsResults, setFriendsResults] = useState<any>(null)
    const [vkTopResults, setVkTopResults] = useState<any>(null)
    const [allTopResults, setAllTopResults] = useState<any>(null)
    const [selectedSegment, setSelectedSegment] = useState<any>(options[0].value)
    const [isOpenTour, setIsOpenTour] = useState<boolean>(false);
    const [isOpenProfileModal, setIsOpenProfileModal] = useState<boolean>(false);
    const [isOpenInfoModal, setIsOpenInfoModal] = useState<boolean>(false);
    const [isLoadingResults, setIsLoadingResults] = useState<boolean>(true);
    const [gamesCountToday, setGameCountToday] = useState<number | null>(null);
    const [isLoadingCount, setIsLoadingCount] = useState<boolean>(true)
    const [onlinePlayersCount, setOnlinePlayersCount] = useState<number>(1);
    const [messageApi, contextHolder] = message.useMessage();
    const [ioSocket, setIoSocket] = useState<Socket | null>(null)

    const cursorRef = useRef<HTMLDivElement>(null)

    const updateGamesCountToday = useCallback(async () => {
        setIsLoadingCount(true)
        try {
            const recordCountToday = await api.getRecordsCountByDay(Date.now())

            setGameCountToday(recordCountToday)
            // @ts-ignore
        } catch (err: AxiosError) {
            if (err.response.status === 406) {
                setGameCountToday(0)
            }
        }
        setIsLoadingCount(false)
    }, [])

    const startSingleGame = async () => {
        setIsOpenFindGameModal(false)
        try {
            const res = await api.startSingleGame(selfField, auth?.bearer)
            setGameId(res.gameId)
            setRightField(new FieldConstructor(res.enemyField))
        } catch (err) {
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
        }
    }

    const onRestart = () => {
        setEnemyData(null)
        setEnemyId(null)
        setGameResult(GameResultEnum.isNonWin)
        setGameId(null)
        setIsLoadingEnemyField(false)
        const randomField = new FieldConstructor().getRandomCompletedField()
        setLeftField(randomField)
        selfField = randomField.field
        updateGamesCountToday()
    }

    const onClickEmptyCell = async (rowIndex: number, columnIndex: number, isRecurse = false) => {
        try {
            setIsLoadingEnemyField(true)
            const res = await api.strikeCell(gameId as string, rowIndex, columnIndex, auth?.bearer)
            if (res.isEndGameOfTimeOut) {
                setGameResult(GameResultEnum.isBotWin)
                setGameId(null)
            } else {
                setRightField(new FieldConstructor(res.botField))
                setLeftField(new FieldConstructor(res.playerField))
                if (res.isKillStrike) {
                    setTimeout(() => {
                        onClickEmptyCell(rowIndex, columnIndex, true)
                    }, 500)
                } else if (res.isPlayerWin) {
                    // @ts-ignore
                    const vkAuthUserId = +auth?.userId?.split('_')[1]

                    const recordCountToday = await api.getRecordsCountByDay(Date.now())
                    setGameCountToday(recordCountToday)

                    setGameResult(GameResultEnum.isPlayerWin)

                    const results1 = await getFriendsResults(vkAuthUserId, auth?.bearer)
                    if (results1) {
                        setFriendsResults(results1)
                    }

                    const results2 = await getVkTopResults(auth?.bearer)
                    if (results2) {
                        setVkTopResults(results2)
                    }

                    const results3 = await getAllTopResults(auth?.bearer)
                    if (results3) {
                        setAllTopResults(results3)
                    }
                } else if (res.isBotWin) {
                    const recordCountToday = await api.getRecordsCountByDay(Date.now())
                    setGameCountToday(recordCountToday)
                    setGameResult(GameResultEnum.isBotWin)
                } else {
                    setIsLoadingEnemyField(false)
                }
            }
        } catch (err) {
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
            Sentry.captureException(err);
            setIsLoadingEnemyField(false)
        }

    }

    const onClickEnemyCell = (rowIndex: number, columnIndex: number) => {
        setIsLoadingEnemyField(true)
        ioSocket?.emit('strike', {
            type: 'strike',
            gameId,
            enemyId,
            userId: auth.userId,
            rowIndex,
            columnIndex,
        })
    }

    const onStart = async () => {
        setIsOpenFindGameModal(true)
        try {
            ioSocket?.emit('startGame', {
                type: 'startGame',
                userId: auth.userId,
                userField: leftField.field
            });
        } catch (err) {
            Sentry.captureException(err);
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
        }
    }

    const onFavorite = async () => {
        try {
            await bridge.send('VKWebAppAddToFavorites')
            messageApi.open({
                type: 'success',
                content: 'Игра добавлена в избранное',
            });
        } catch (err) {
            messageApi.open({
                type: 'error',
                content: 'Не удалось добавить игру в избранное',
            });
        }

    }

    const onShare = async () => {
        try {
            await bridge.send("VKWebAppShowInviteBox", {})
            messageApi.open({
                type: 'success',
                content: 'Вы пригласили друга',
            });
        } catch (err) {
            Sentry.captureException(err);
            messageApi.open({
                type: 'error',
                content: 'Не удалось пригласить друга',
            });
        }
    }
    const onChangeSegment = async (val: SegmentedValue) => {
        setIsLoadingResults(true)
        try {
            const newOption = options.find(o => o.value === val)

            if (newOption) {
                if (val === options[1].value && !vkTopResults) {
                    const results = await getVkTopResults(auth?.bearer)
                    // @ts-ignore
                    setVkTopResults(results)
                }
                if (val === options[2].value && !allTopResults) {
                    const results = await getAllTopResults(auth?.bearer)
                    // @ts-ignore
                    setAllTopResults(results)
                }
                setSelectedSegment(newOption.value)
            }
        } catch (err) {
            Sentry.captureException(err);
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: 'Не удалось загрузить результаты',
            });
        }

        setIsLoadingResults(false)
    }

    const onStartSingleGameClick = () => {
        ioSocket?.emit('leaveWaitingList', true)
    }

    const onEndTime = () => {
        if (!enemyId || enemyId === 'computer') {
            setEnemyId(null)
            setEnemyData(null)

            setGameResult(GameResultEnum.isBotWin)
        }
    }

    useWSConnection({
        auth,
        setIoSocket,
        setGameId,
        setLoadingMessage: () => {},
        setIsLoadingEnemy: setIsLoadingEnemyField,
        setIsLoadingGame: setIsOpenFindGameModal,
        setEnemyData,
        setEnemyId,
        setEnemyField: setRightField,
        setSelfField: setLeftField,
        setGameResult,
        setIsVisibleHeader: () => {},
        onLeaveWaitingListCb: startSingleGame,
        setOnlinePlayersCount
    })

    useEffect(() => {
        (async function () {
            try {
                // @ts-ignore
                if (auth.bearer && auth.userId) {
                    updateGamesCountToday()

                    const vkAuthUserId = +auth.userId?.split('_')[1]

                    const results = await getFriendsResults(vkAuthUserId, auth?.bearer)

                    const vkStorage = await bridge.send('VKWebAppStorageGet', {keys: ['learningDidShow']})
                    const learningDidShow = vkStorage.keys.find(el => el.key === 'learningDidShow')

                    setFriendsResults(results)
                    setIsAppLoading()
                    if (!learningDidShow?.value) {
                        setIsOpenTour(true)
                        await bridge.send('VKWebAppStorageSet', {
                            key: 'learningDidShow',
                            value: 'true'
                        })
                    }
                }
            } catch (err) {
                Sentry.captureException(err);
                console.log(err)
                setIsAppLoading()
                messageApi.open({
                    type: 'error',
                    // @ts-ignore
                    content: 'Не удалось загрузить результаты друзей',
                });
            }
            setIsLoadingResults(false)
        })()
    }, [auth])

    return <div className={clsx(styles.appContainer)}>
        {contextHolder}
        <FindGameModal isOpen={isOpenFindGameModal} onClickBtn={onStartSingleGameClick}/>
        {!gameId && <div className={styles.nowStatsTags}>
            <TodayGameCount isLoadingCount={isLoadingCount} count={gamesCountToday || 0}/>
            <OnlinePlayersCount count={onlinePlayersCount}/>
        </div>}
        <FieldsGameBlock
            onClickEmptyCell={enemyId ? onClickEnemyCell : onClickEmptyCell}
            isLoadingBotField={isLoadingEnemyField}
            setLeftField={(fCons) => {
                setLeftField(fCons)
                selfField = fCons.field
            }}
            rightField={rightField}
            leftField={leftField}
            gameIsPlaying={!!gameId}
            cursorRef={cursorRef}
        />
        <div className={styles.btnContainer}>
            {!gameId && <Button title="Профиль" className={styles.favoriteBtn} type="primary" id="ShareGameButton__VKGames"
                                onClick={() => setIsOpenProfileModal(true)}><UserOutlined /></Button>}
            {!gameId && <Button className={styles.startBtn} type="primary" id="StartGameButton__VKGames"
                                onClick={onStart}>Старт <PlayCircleOutlined/></Button>}
            {!gameId && <Button className={styles.favoriteBtn} type="primary" id="FavoriteGameButton__VKGames"
                                onClick={onFavorite}><StarOutlined/></Button>}
            {!gameId && <Button className={styles.favoriteBtn} type="primary" id="ShareGameButton__VKGames"
                                onClick={onShare}><ShareAltOutlined/></Button>}
            {!gameId && <Button className={styles.favoriteBtn} type="primary" id="InfoGameButton__VKGames"
                                onClick={() => setIsOpenInfoModal(true)}><InfoCircleOutlined /></Button>}
        </div>
        {!gameId && <ChatResults
            showTabs
            options={options}
            showShareBtn={false}
            title=""
            results={getCurrentResults(selectedSegment, friendsResults, vkTopResults, allTopResults) || []}
            selectedSegment={selectedSegment}
            onChangeSegment={onChangeSegment}
            isLoadingResults={isLoadingResults}
        />}
        {gameId && (gameResult === GameResultEnum.isNonWin) && <Timer
            restartProp={rightField}
            startTime={AppConfig.singleGame.interval / 1000}
            onEndTime={onEndTime}
        />}
        {gameId && <EnemyInfoBlock enemyData={enemyData}/>}
        <Tour open={isOpenTour} rootClassName={styles.tourMainContentContainer} onClose={() => setIsOpenTour(false)}
              steps={getSteps()}/>
        <ResultModal
            isOpen={gameResult !== GameResultEnum.isNonWin}
            gameResult={gameResult}
            leftField={leftField}
            rightField={rightField}
            onOk={onRestart}
            gameId={gameId}
            userId={auth.userId}
            showShareBtn
        />
        <ProfileModal
            userId={auth.userId}
            onClose={() => setIsOpenProfileModal(false)}
            isOpen={isOpenProfileModal}
        />
        <InfoModal isOpen={isOpenInfoModal} onClose={() => setIsOpenInfoModal(false)}/>
    </div>
}

export default VKGamePage
