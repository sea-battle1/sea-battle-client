import {UserType} from "../../core/types/userTypes";

export const getUserName = (playerInfo: UserType | null | undefined , playerId: string): string => {
    if (!playerInfo) return playerId
    return `${playerInfo.first_name}`
}