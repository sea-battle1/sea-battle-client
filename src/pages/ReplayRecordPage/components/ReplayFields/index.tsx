import React from "react";
import {Field} from "../../../../components/Field";
import {FieldTypesEnum} from "../../../../core/constants/enums";
import styles from './styles.module.css'
import {FieldHeader} from "../../../../components/FieldHeader";
import {ReplayFieldConstructor} from "../../ReplayFieldConstructor";
import {useWindowsWidth} from "../../../../core/helpers/useWindowWidth";
import clsx from "clsx";

type ReplayFieldsProps = {
    leftField: ReplayFieldConstructor,
    rightField: ReplayFieldConstructor,
    leftPlayerName: string,
    rightPlayerName: string,
    fieldsBlockWidth?: number,
    leftHeaderClassName?: string,
    rightHeaderClassName?: string,
}

export const ReplayFields: React.FC<ReplayFieldsProps> = ({
                                                              leftField,
                                                              rightField,
                                                              leftPlayerName,
                                                              rightPlayerName,
                                                              fieldsBlockWidth = 690,
    leftHeaderClassName,
    rightHeaderClassName,
                                                          }) => {

    const width = useWindowsWidth()

    return <>
        {(width < 600) && <FieldHeader
            className={clsx(styles.mobileShipsInfo, leftHeaderClassName)}
            name={leftPlayerName}
            killedShips={leftField.killedShips}
            color={'green'}
        />}
        <div style={{
            zoom: width < 600 ? width / fieldsBlockWidth : 1,
            transform: width < 600 ? 'translateX(-5px)' : 'none'
        }} className={styles.replayFieldsContainer}>
            <div className={styles.replayField}>
                {(width >= 600) && <FieldHeader
                    className={clsx(styles.fieldHeaderReplay, leftHeaderClassName)}
                    name={leftPlayerName}
                    killedShips={leftField.killedShips}
                    color={'green'}
                />}
                <Field field={leftField.field} fieldType={FieldTypesEnum.self}/>
            </div>
            <div className={styles.replayField}>
                {(width >= 600) && <FieldHeader
                    className={clsx(styles.fieldHeaderReplay, rightHeaderClassName)}
                    name={rightPlayerName}
                    killedShips={rightField.killedShips}
                    color={'green'}
                />}
                <Field field={rightField.field} fieldType={FieldTypesEnum.self}/>
            </div>
        </div>
        {(width < 600) && <FieldHeader
            className={ clsx(styles.mobileShipsInfo, rightHeaderClassName) }
            name={rightPlayerName}
            killedShips={rightField.killedShips}
            color={'green'}
        />}
    </>
}