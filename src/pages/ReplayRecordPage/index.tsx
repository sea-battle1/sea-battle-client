import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import * as Sentry from "@sentry/react";
import {PlayCircleOutlined} from "@ant-design/icons";
import {Button, message} from "antd";
import {api} from "../../core/api";
import {ReplayRecordType, StrikeType} from "../../core/types/fieldTypes";
import {ReplayFields} from "./components/ReplayFields";
import {UserType} from "../../core/types/userTypes";
import {getUserName} from "./utils";
import {ReplayFieldConstructor} from "./ReplayFieldConstructor";
import styles from './styles.module.css'

type ExpandedStrikeType = StrikeType & { side: 'left' | 'right' }

const ReplayRecordPage = () => {
    const [gameRecord, setGameRecord] = useState<ReplayRecordType | null>(null)
    const [leftPlayer, setLeftPlayer] = useState<UserType | null>(null)
    const [rightPlayer, setRightPlayer] = useState<UserType | null>(null)
    const [leftField, setLeftField] = useState<ReplayFieldConstructor>()
    const [rightField, setRightField] = useState<ReplayFieldConstructor>()
    const [concatStrikes, setConcatStrikes] = useState<ExpandedStrikeType[]>([])
    const [isPlayingRecord, setIsPlayingRecord] = useState<boolean>(false)
    const params = useParams();

    const playRecord = () => {
        setIsPlayingRecord(true)
        setLeftField(new ReplayFieldConstructor(gameRecord?.startCreatorPlayerField.map(row => row.map(col => col))))
        setRightField(new ReplayFieldConstructor(gameRecord?.startSecondPlayerField.map(row => row.map(col => col))))
        concatStrikes.forEach((strike, i) => {
            setTimeout(() => {
                if (strike.side === 'right') {
                    setLeftField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                } else {
                    setRightField(prev => new ReplayFieldConstructor(prev?.updateField(strike.row, strike.column).field))
                }
                if (i === concatStrikes.length - 1) {
                    setIsPlayingRecord(false)
                }
            }, i * 100)
        })
    }

    useEffect(() => {
        (async function () {
            try {
                if (params.gameId) {
                    const res = await api.getGameRecord(params.gameId)
                    if (res.creatorPlayerId !== 'computer') {
                        try {
                            const leftPlayerData = await api.getUserInfo(res.creatorPlayerId)
                            setLeftPlayer(leftPlayerData)
                        } catch (err) {
                            console.error(err)
                        }
                    } else {

                    }
                    if (res.secondPlayerId !== 'computer') {
                        try {
                            const rightPlayerData = await api.getUserInfo(res.secondPlayerId)
                            setRightPlayer(rightPlayerData)
                        } catch (err) {
                            console.error(err)
                        }
                    } else {

                    }
                    setLeftField(new ReplayFieldConstructor(res.startCreatorPlayerField.map(row => row.map(col => col))))
                    setRightField(new ReplayFieldConstructor(res.startSecondPlayerField.map(row => row.map(col => col))))
                    setGameRecord(res)
                    const leftStrikes: ExpandedStrikeType[] = res?.creatorPlayerStrikes.map(el => ({
                        ...el,
                        side: 'left'
                    })) || []
                    const rightStrikes: ExpandedStrikeType[] = res?.secondPlayerStrikes.map(el => ({
                        ...el,
                        side: 'right'
                    })) || []
                    const unionStrikes = [...leftStrikes, ...rightStrikes].sort((a, b) => a.timeFromStart - b.timeFromStart)
                    setConcatStrikes(unionStrikes)
                }
            } catch (err) {
                Sentry.captureException(err);
                message.open({
                    type: 'error',
                    content: 'Не удалось загрузить данные пользователя'
                })
                console.error(err)
            }
        })()
    }, [])

    return <div className={styles.pageContainer}>
        {gameRecord && <div className={styles.replayData}>
            Дата боя: <span className={styles.replayDurationValue}>{(new Date(gameRecord.gameStartTime).toLocaleString())}</span>
        </div>}
        {gameRecord && <div className={styles.replayDuration}>Продолжительность: <span className={styles.replayDurationValue}>
            {((new Date(gameRecord.gameEndTime).valueOf() - new Date(gameRecord.gameStartTime).valueOf())/1000).toFixed(1)} сек
        </span></div>}
        {gameRecord && <div className={styles.replayDuration}>Победитель: <span className={styles.replayDurationValue}>
            {getUserName(
                gameRecord.winnerId === gameRecord.creatorPlayerId ? leftPlayer : rightPlayer
                , gameRecord.winnerId === gameRecord.creatorPlayerId ? gameRecord.creatorPlayerId : gameRecord.secondPlayerId
            )}
        </span></div>}
        {gameRecord && leftField && rightField && <ReplayFields
            leftField={leftField}
            rightField={rightField}
            leftPlayerName={getUserName(leftPlayer, gameRecord.creatorPlayerId)}
            rightPlayerName={getUserName(rightPlayer, gameRecord.secondPlayerId)}
        />}
        <div className={styles.btnContainer}>
            <Button disabled={isPlayingRecord} onClick={playRecord} type="primary">Воспроизвести <PlayCircleOutlined/></Button>
        </div>
    </div>
}

export default ReplayRecordPage
