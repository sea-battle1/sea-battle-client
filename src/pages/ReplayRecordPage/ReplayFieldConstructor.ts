import {CheckShipType, FieldConstructor} from "../../core/helpers/FieldConstructor";
import {FieldType} from "../../core/types/fieldTypes";
import {CellValueEnum} from "../../core/constants/enums";

export class ReplayFieldConstructor extends FieldConstructor {
    constructor(field?: FieldType) {
        super(field);
    }

    updateField (rowIndex: number, columnIndex: number) {
        let value = CellValueEnum.miss
        if (this.field[rowIndex][columnIndex] === CellValueEnum.ship) {
            value = CellValueEnum.kill
        }
        this.field[rowIndex][columnIndex] = value

        if (value === CellValueEnum.kill) {
            const currentShip = this.ships.find(ship => ship.find(cell => cell.row === rowIndex && cell.col === columnIndex)) as CheckShipType[]
            if (currentShip.every(cell => this.field[cell.row][cell.col] === CellValueEnum.kill)) {
                this.setMissValues(currentShip.map(cell => [cell.row, cell.col]))
            }
        }
        return this
    }
}
