import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {FieldType} from "../../core/types/fieldTypes";
import {CellValueEnum} from "../../core/constants/enums";

export class SimulateFieldConstructor extends FieldConstructor {

    lastUpdateIsKilled: boolean = false

    constructor(field?: FieldType) {
        super(field);
    }

    private getRandomValidCell (): number[] {
        const killCoords = this.field.reduce((acc: string[], row, rowIndex): string[]  => {
            const copy = [...acc]
            row.forEach((col, colIndex) => {
                if (col === CellValueEnum.kill) {
                    copy.push(`${rowIndex}-${colIndex}`)
                }
            })
            return copy
        }, [])
        let mappedField
        if (killCoords) {
            for (let i = 0; i < killCoords.length; i++) {
                const [row, col] = killCoords[i].split('-').map(str => +str)
                const isHaveNeibShip = this.checkNeibCell(row, col, CellValueEnum.ship)
                if (isHaveNeibShip) {
                    mappedField = this.getNeibVariantsForKilledCell(row, col)
                    break
                }
            }
        }
        if (!mappedField) {
            mappedField = this.field.reduce((acc: string[], row, rowIndex): string[] => {
                const copy = [...acc]
                row.forEach((col, colIndex) => {
                    if (col !== CellValueEnum.kill && col !== CellValueEnum.miss) {
                        copy.push(`${rowIndex}-${colIndex}`)
                    }
                })
                return copy
            }, [])
        }
        const randomNum = Math.floor(Math.random() * mappedField.length)
        const randomCell = mappedField[randomNum]
        return randomCell.split('-').map((str: string) => +str)
    }

    private updateField (rowIndex: number, columnIndex: number) {
        let value = CellValueEnum.miss
        if (this.field[rowIndex][columnIndex] === CellValueEnum.ship) {
            value = CellValueEnum.kill
        }
        this.field[rowIndex][columnIndex] = value

        const coords = this.checkCell(rowIndex, columnIndex)

        if (coords && value === CellValueEnum.kill) {
            this.setMissValues(coords)
        }
    }

    strikeRandomCell () {
        this.lastUpdateIsKilled = false
        const [row, col] = this.getRandomValidCell()
        this.updateField(row, col)

        this.lastUpdateIsKilled = this.field[row][col] === CellValueEnum.kill
        return this
    }
}