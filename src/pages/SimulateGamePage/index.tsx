import React, {useEffect, useLayoutEffect, useState} from "react";
import {Button, Radio, RadioChangeEvent} from "antd";
import {Field} from "../../components/Field";
import {BetsEnum, FieldTypesEnum, GameResultEnum} from "../../core/constants/enums";
import {AuthAppStateType} from "../../core/types/authTypes";
import {CheckShipsBlock} from "../../components/CheckShipsBlock";
import {SimulateFieldConstructor} from "./SimulateFieldConstructor";
import {api} from "../../core/api";
import styles from './styles.module.css'
import {useWindowsWidth} from "../../core/helpers/useWindowWidth";

type SimulateGamePageProps = {
    auth: AuthAppStateType
}

const SimulateGamePage: React.FC<SimulateGamePageProps> = ({auth}) => {
    const [leftField, setLeftField] = useState<SimulateFieldConstructor>(new SimulateFieldConstructor())
    const [rightField, setRightField] = useState<SimulateFieldConstructor>(new SimulateFieldConstructor())
    const [isStartGame, setIsStartGame] = useState<boolean>(false)
    const [intervalValue, setIntervalValueValue] = useState<NodeJS.Timer | null>(null)
    const [gameResult, setGameResult] = useState<GameResultEnum>(GameResultEnum.isNonWin)
    const [betId, setBetId] = useState<string | null>(null)
    const [radioValue, setRadioValue] = useState<BetsEnum | null>(null);

    const width = useWindowsWidth()

    const onChange = (e: RadioChangeEvent) => {
        setRadioValue(e.target.value);
    };

    const onStart = async () => {
        try {
            setIsStartGame(true)
            setLeftField(leftField.getRandomCompletedField())
            setRightField(rightField.getRandomCompletedField())
            if (radioValue && auth.userId) {
                const res = await api.placeBet(auth.userId, radioValue)
                if (res) {
                    setBetId(res.betId)
                }
            }
            let i = 0
            const interval = setInterval(() => {
                if (i % 2 === 0) {
                    setRightField((field: SimulateFieldConstructor) => {
                        field.strikeRandomCell()
                        if (!field.isHaveShip) {
                            setGameResult(GameResultEnum.isBotWin)
                        }
                        if (field.lastUpdateIsKilled) {
                            i++
                        }

                        return new SimulateFieldConstructor(field.field)
                    })
                } else {
                    setLeftField((field: SimulateFieldConstructor) => {
                        field.strikeRandomCell()
                        if (!field.isHaveShip) {
                            setGameResult(GameResultEnum.isBotWin)
                        }
                        if (field.lastUpdateIsKilled) {
                            i++
                        }

                        return new SimulateFieldConstructor(field.field)
                    })
                }
            }, 1)
            setIntervalValueValue(interval)
        } catch (err) {
            console.error(err)
        }
    }

    const onStop = () => {
        setIsStartGame(false)
        clearInterval(intervalValue as NodeJS.Timer)
    }

    const onRestart = () => {
        setIsStartGame(false)
        setLeftField(new SimulateFieldConstructor())
        setRightField(new SimulateFieldConstructor())
        setGameResult(GameResultEnum.isNonWin)
        setRadioValue(null)
        setBetId(null)
    }

    useLayoutEffect(() => {
        try {
            (async function () {
                if (gameResult !== GameResultEnum.isNonWin) {
                    clearInterval(intervalValue as NodeJS.Timer)
                    if (betId && auth.userId) {
                        await api.resultBet(
                            auth.userId,
                            gameResult === GameResultEnum.isBotWin ? BetsEnum.leftBot : BetsEnum.rightBot,
                            betId
                        )
                    }
                }
            })()
        } catch (err) {
            console.error(err)
        }

    }, [gameResult])

    useEffect(() => {
        return () => {
            if (intervalValue) {
                clearInterval(intervalValue)
            }
        }
    }, [])

    return <div className={styles.simulateGameCont}>
        <div className={styles.fieldsCont}>
            {width > 1000 && <CheckShipsBlock ships={leftField.ships}/>}
            <Field field={leftField.field} fieldType={FieldTypesEnum.self} isLoading/>
            <Field field={rightField.field} fieldType={FieldTypesEnum.self} isLoading/>
            {width > 1000 && <CheckShipsBlock ships={rightField.ships}/>}
        </div>
        <div className={styles.radioContainer}>
            <h3>Попробуйте угадать кто выиграет</h3>
            <Radio.Group className={styles.radioBtns} onChange={onChange} value={radioValue}>
                <Radio value={BetsEnum.leftBot}>Левый бот</Radio>
                <Radio value={BetsEnum.rightBot}>Правый бот</Radio>
            </Radio.Group>
        </div>
        {gameResult === GameResultEnum.isNonWin && <>
            <Button disabled={isStartGame} className={styles.startBtn} onClick={onStart} type="primary">Начать</Button>
            <Button disabled={!isStartGame} className={styles.startBtn} onClick={onStop} type="primary">Стоп</Button>
        </>}
        {gameResult !== GameResultEnum.isNonWin && <Button className={styles.startBtn} onClick={onRestart} type="primary">Начать заного</Button>}
        {gameResult === GameResultEnum.isBotWin && <span className={styles.winText}>Левый игрок победил</span>}
        {gameResult === GameResultEnum.isPlayerWin && <span className={styles.winText}>Правый игрок победил</span>}
    </div>
}

export default SimulateGamePage