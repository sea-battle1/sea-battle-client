import {ColumnsType} from "antd/es/table";
import {GameRecordTypes, RecordReplayFromList, ReplayRecordType} from "../../core/types/fieldTypes";
import {NavLink} from "react-router-dom";
import {Button, Tag} from "antd";
import styles from "../ProfilePage/components/ReplaysList/styles.module.css";
import React from "react";

export const columns: ColumnsType<ReplayRecordType> = [
    {
        title: 'Дата',
        dataIndex: 'gameStartTime',
        key: 'gameStartTime',
        render: (value) => <span>{(new Date(value)).toLocaleString()}</span>
    },
    {
        title: '',
        key: 'gameId',
        dataIndex: 'gameId',
        width: 132,
        render: (gameId) => <NavLink to={`/replayGame/${gameId}`}><Button className={styles.playRecordBtn}
                                                                          type="primary">Воспроизвести</Button></NavLink>
    },
    {
        title: 'Тип игры',
        dataIndex: 'type',
        key: 'type',
        render: (value) => <Tag
            color={value === GameRecordTypes.mp ? 'blue' : 'yellow'}
        >{value === GameRecordTypes.mp ? 'Многопользовательская' : 'Одиночная'}</Tag>
    },
    {
        title: 'Время игры',
        dataIndex: 'gameEndTime',
        key: 'gameEndTime',
        // @ts-ignore
        sorter: (a, b) => (a.gameEndTime - a.gameStartTime) - (b.gameEndTime - b.gameStartTime),
        render: (value, otherValue: any) => <span>{((new Date(value).valueOf() - new Date(otherValue.gameStartTime).valueOf())/ 1000).toFixed(1)} сек.</span>
    },
    {
        title: 'Создатель игры',
        dataIndex: 'creatorPlayerId',
        key: 'creatorPlayerId',
        render: (value) => <NavLink to={`/profile/${value}`}>Посмотреть</NavLink>
    },
    {
        title: 'Второй игрок',
        dataIndex: 'secondPlayerId',
        key: 'secondPlayerId',
        render: (value) => value !== 'computer' ? <NavLink to={`/profile/${value}`}>Посмотреть</NavLink> : ''
    }
];
