import React, {useEffect, useState} from "react";
import {api} from "../../core/api";
import * as Sentry from "@sentry/react";
import {message, Table, Tag} from "antd";
import {ReplayRecordType} from "../../core/types/fieldTypes";
import styles from "../ProfilePage/components/ReplaysList/styles.module.css";
import {columns} from "./utils";

const RecordsListPage = () => {
    const [records, setRecords] = useState<ReplayRecordType[] | null>(null)
    const [currentPage, setCurrentPage] = useState(1)
    const [count, setCount] = useState(0)
    useEffect(() => {
        (async function () {
            try {
                    const {count, records} = await api.getRecords({
                        page: 1,
                    })
                    if (records) {
                        setRecords(records)
                        setCount(count)
                    }
            } catch (err) {
                Sentry.captureException(err);
                message.open({
                    type: 'error',
                    content: 'Не удалось загрузить данные пользователя'
                })
                console.error(err)
            }
        })()
    }, [])
    return  <Table
        scroll={{x: true}}
        bordered
        className={styles.tableReplays}
        columns={columns}
        dataSource={records || []}
        rowKey="gameId"
        size="small"
        pagination={{
            onChange: async (page) => {
                try {
                    const {count, records} = await api.getRecords({
                        page: page,
                    })
                    if (records) {
                        setRecords(records)
                        setCount(count)
                    }
                    setCurrentPage(page)

                } catch (err) {
                    Sentry.captureException(err);
                    message.open({
                        type: 'error',
                        content: 'Не удалось загрузить данные пользователя'
                    })
                    console.error(err)
                }
            },
            current: currentPage,
            pageSize: 20,
            total: count,
            showTotal: () => <Tag color="purple-inverse">{count} игр</Tag>,
            totalBoundaryShowSizeChanger: count
        }}
    />
}

export default RecordsListPage
