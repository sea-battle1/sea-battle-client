import React from "react";
import {useNavigate} from "react-router-dom";
import {AppleDownloadBtn} from "../../components/AppleDownloadBtn";
import {RuStoreDownloadBtn} from "../../components/RuStoreDownloadBtn";
import styles from './styles.module.css'
// @ts-ignore
import screenPhone from './../../core/assets/images/phoneScreen.png'

const HomePage = () => {
    const navigate = useNavigate()
    const onClickApple = () => {
        window.open('https://apps.apple.com/ru/app/%D0%BC%D0%BE%D1%80%D1%81%D0%BA%D0%BE%D0%B9-%D0%B1%D0%BE%D0%B9-battleships/id6446815384')
    }
    return <>
        <h1>
            Морской бой
        </h1>
        <h2>
            Реализация классической игры Морской Бой
        </h2>
        <section>
            <h4 className="modesGameTitle">Доступные режимы:</h4>
            <ul className="modesGame">
                <li onClick={() => navigate('/singleGame')} className="modeItem">
                    <article>
                        <h3 className="modeName">Одиночная игра</h3>
                        <p>Игра с компьютером</p>
                    </article>
                </li>
                <li onClick={() => navigate('/multiplayerGame')} className="modeItem">
                    <article>
                        <h3 className="modeName">Игра с другим игроком игра</h3>
                        <p>Мультиплеерная игра с другим игроком</p>
                    </article>
                </li>
                <li onClick={() => navigate('/simulateGame')} className="modeItem">
                    <article>
                        <h3 className="modeName">Игра ботов</h3>
                        <p>Симуляция игры компьютером</p>
                    </article>
                </li>
            </ul>
        </section>
        <section className={styles.mobileApps}>
            <h3 className={styles.mobileAppsTitle}>Скачайте мобильное приложение для своего устройства</h3>
            <div className={styles.mobileAppsContent}>
                <div className={styles.mobileAppsBtns}>
                    <AppleDownloadBtn onClick={onClickApple}/>
                    <RuStoreDownloadBtn/>
                </div>
                <img width={200} src={screenPhone}/>
            </div>
        </section>
    </>
}

export default HomePage