import React, {useCallback, useEffect, useRef, useState} from "react";
import clsx from "clsx";
import {Socket} from "socket.io-client";
import * as Sentry from "@sentry/react";
import {Button, message, Tour} from "antd";
import {PlayCircleOutlined, ShareAltOutlined, UserOutlined} from '@ant-design/icons';
import {SegmentedValue} from "antd/lib/segmented";
import {GameResultEnum} from "../../core/constants/enums";
import {Timer} from "../../components/Timer";
import {AppConfig} from "../../appConfig";
import {api} from "../../core/api";
import {FieldConstructor} from "../../core/helpers/FieldConstructor";
import {AuthAppStateType, AuthTypes} from "../../core/types/authTypes";
import {ResultModal} from "../../components/ResultModal";
import {FindGameModal} from "../../components/FindGameModal";
import {ChatResults} from "./components/ChatResults";
import {ChatUserResultType} from "../../core/types/userTypes";
import {FieldsGameBlock} from "../../components/FieldsGameBlock";
import {onMoveSocket} from "../../core/helpers/prepareMouseCoordinates";
import {options, getCurrentResults, getAllTopResults, getTGTopResults, steps} from "./utils";
import {setIsAppLoading} from "../../core/helpers/setIsAppLoading";
import {EnemyInfoBlock} from "../../components/EnemyInfoBlock";
import {TodayGameCount} from "../../components/TodayGameCount";
import {useWSConnection} from "../../core/helpers/useWSConnection";
import {OnlinePlayersCount} from "../../components/OnlinePlayersCount";
import styles from './styles.module.css'
import {FieldType} from "../../core/types/fieldTypes";
import {ProfileModal} from "../../components/ProfileModal";
import {AxiosError} from "axios";

type TGGamePageProps = {
    auth: AuthAppStateType
}

let selfField: FieldType

const TGGamePage: React.FC<TGGamePageProps> = ({auth}) => {
    const [leftField, setLeftField] = useState<FieldConstructor>(() => {
        const firstRandomField = new FieldConstructor().getRandomCompletedField()
        if (!selfField) {
            selfField = firstRandomField.field
        }
        return firstRandomField
    })
    const [rightField, setRightField] = useState<FieldConstructor>(new FieldConstructor())
    const [gameId, setGameId] = useState<string | null>(null)
    const [isLoadingEnemyField, setIsLoadingEnemyField] = useState<boolean>(false)
    const [gameResult, setGameResult] = useState<GameResultEnum>(GameResultEnum.isNonWin)
    const [isOpenFindGameModal, setIsOpenFindGameModal] = useState<boolean>(false)
    const [enemyId, setEnemyId] = useState<string | null>(null)
    const [enemyData, setEnemyData] = useState<any | null>(null)
    const [chatResults, setChatResults] = useState<ChatUserResultType[] | null>(null)
    const [tgTopResults, setTgTopResults] = useState<any>(null)
    const [allTopResults, setAllTopResults] = useState<any>(null)
    const [isLoadingResults, setIsLoadingResults] = useState<boolean>(true)
    const [selectedSegment, setSelectedSegment] = useState<any>(options[0].value)
    const [isOpenTour, setIsOpenTour] = useState<boolean>(false);
    const [gamesCountToday, setGameCountToday] = useState<number | null>(null);
    const [ioSocket, setIoSocket] = useState<Socket | null>(null)
    const [onlinePlayersCount, setOnlinePlayersCount] = useState<number>(1);
    const [isLoadingCount, setIsLoadingCount] = useState<boolean>(true)
    const [isOpenProfileModal, setIsOpenProfileModal] = useState<boolean>(false);
    const [messageApi, contextHolder] = message.useMessage();

    const cursorRef = useRef<HTMLDivElement>(null)

    const updateGamesCountToday = useCallback(async () => {
        setIsLoadingCount(true)
        try {
            const recordCountToday = await api.getRecordsCountByDay(Date.now())

            setGameCountToday(recordCountToday)
            // @ts-ignore
        } catch (err: AxiosError) {
            if (err.response.status === 406) {
                setGameCountToday(0)
            }
        }
        setIsLoadingCount(false)
    }, [])

    const startSingleGame = async () => {
        try {
            setIsOpenFindGameModal(false)
            // @ts-ignore
            const res = await api.startSingleGame(selfField)
            setGameId(res.gameId)
            setRightField(new FieldConstructor(res.enemyField))
        } catch (err) {
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
        }
    }

    const onRestart = () => {
        setEnemyData(null)
        setGameId(null)
        setEnemyId(null)
        setGameResult(GameResultEnum.isNonWin)
        setIsLoadingEnemyField(false)
        const randomField = new FieldConstructor().getRandomCompletedField()
        setLeftField(randomField)
        selfField = randomField.field
        updateGamesCountToday()

    }

    const onClickEmptyCell = async (rowIndex: number, columnIndex: number) => {
        try {
            setIsLoadingEnemyField(true)
            const res = await api.strikeCell(gameId as string, rowIndex, columnIndex)
            if (res.isEndGameOfTimeOut) {
                setGameResult(GameResultEnum.isBotWin)
                setGameId(null)
            } else {
                setRightField(new FieldConstructor(res.botField))
                setLeftField(new FieldConstructor(res.playerField))
                if (res.isKillStrike) {
                    setTimeout(() => {
                        onClickEmptyCell(rowIndex, columnIndex)
                    }, 500)
                } else if (res.isPlayerWin) {
                    const recordCountToday = await api.getRecordsCountByDay(Date.now())
                    setGameCountToday(recordCountToday)

                    setGameResult(GameResultEnum.isPlayerWin)
                    if (auth.authType === AuthTypes.TG && auth?.userId && auth?.startGameId) {
                        await api.tgWin(auth?.userId, auth?.startGameId)
                    }
                } else if (res.isBotWin) {
                    const recordCountToday = await api.getRecordsCountByDay(Date.now())
                    setGameCountToday(recordCountToday)

                    setGameResult(GameResultEnum.isBotWin)
                } else {
                    setIsLoadingEnemyField(false)
                }
            }
        } catch (err) {
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
            Sentry.captureException(err);
            setIsLoadingEnemyField(false)
        }

    }

    const onEndTime = () => {
        if (cursorRef?.current) {
            cursorRef.current.style.display = 'none'
        }
        if (!enemyId || enemyId === 'computer') {
            setEnemyId(null)
            setEnemyData(null)
            setGameResult(GameResultEnum.isBotWin)
        }
    }

    const onClickEnemyCell = (rowIndex: number, columnIndex: number) => {
        setIsLoadingEnemyField(true)
        ioSocket?.emit('strike', {
            type: 'strike',
            gameId,
            enemyId,
            userId: auth.userId,
            rowIndex,
            columnIndex,
        });
    }

    const onStart = async () => {
        setIsOpenFindGameModal(true)
        try {
            ioSocket?.emit('startGame', {
                type: 'startGame',
                userId: auth.userId,
                userField: leftField.field
            });
        } catch (err) {
            Sentry.captureException(err);
            messageApi.open({
                type: 'error',
                // @ts-ignore
                content: err?.message || 'Что-то пошло не так',
            });
        }
    }

    const onStartSingleGameClick = () => {
        ioSocket?.emit('leaveWaitingList', true)
    }


    const onChangeSegment = async (val: SegmentedValue) => {
        setIsLoadingResults(true)
        try {
            const newOption = options.find(o => o.value === val)
            if (newOption) {
                if (val === options[1].value && !tgTopResults) {
                    const results = await getTGTopResults()
                    // @ts-ignore
                    setTgTopResults(results)
                }
                if (val === options[2].value && !allTopResults) {
                    const results = await getAllTopResults()
                    // @ts-ignore
                    setAllTopResults(results)
                }
                setSelectedSegment(newOption.value)
            }
        } catch (err) {
            console.log(err)
        }

        setIsLoadingResults(false)
    }

    const onShare = () => {
        // @ts-ignore
        window.TelegramGameProxy.shareScore()
    }

    useWSConnection({
        auth,
        setIoSocket,
        setGameId,
        setLoadingMessage: () => {},
        setIsLoadingEnemy: setIsLoadingEnemyField,
        setIsLoadingGame: setIsOpenFindGameModal,
        setEnemyData,
        setEnemyId,
        setEnemyField: setRightField,
        setSelfField: setLeftField,
        setGameResult,
        setIsVisibleHeader: () => {},
        onLeaveWaitingListCb: startSingleGame,
        setOnlinePlayersCount
    })

    useEffect(() => {
        (async function () {
            try {
                updateGamesCountToday()
                if (auth.userId && auth.startGameId) {
                    const recordCountToday = await api.getRecordsCountByDay(Date.now())
                    setGameCountToday(recordCountToday)

                    const res = await api.getChatResult(auth.userId, auth.startGameId)
                    if (Array.isArray(res)) {
                        setChatResults(res)
                    }
                }
            } catch (err) {
                console.error(err)
            }
            setIsLoadingResults(false)
            setIsAppLoading()
            const learningDidShow = localStorage.getItem('learningDidShow')
            if (!learningDidShow) {
                setIsOpenTour(true)
                localStorage.setItem('learningDidShow', 'true')
            }
        })()
    }, [auth])

    return <div className={clsx(styles.appContainer)}>
        {!gameId && <div className={styles.nowStatsTags}>
            <TodayGameCount isLoadingCount={isLoadingCount} count={gamesCountToday || 0}/>
            <OnlinePlayersCount count={onlinePlayersCount}/>
        </div>}
        <FindGameModal
            isOpen={isOpenFindGameModal}
            onClickBtn={onStartSingleGameClick}
        />
        <FieldsGameBlock
            onClickEmptyCell={enemyId ? onClickEnemyCell : onClickEmptyCell}
            isLoadingBotField={isLoadingEnemyField}
            setLeftField={(fCons) => {
                setLeftField(fCons)
                selfField = fCons.field
            }}
            rightField={rightField}
            leftField={leftField}
            gameIsPlaying={!!gameId}
            cursorRef={cursorRef}
        />
        <div className={styles.btnContainer}>
            {!gameId && <Button title="Профиль" className={styles.favoriteBtn} type="primary" id="ShareGameButton__VKGames"
                                onClick={() => setIsOpenProfileModal(true)}><UserOutlined /></Button>}
            {!gameId && <Button className={styles.startBtn} id="StartGameButton__TGGames" type="primary" onClick={onStart}>Старт <PlayCircleOutlined /></Button>}
            {!gameId && <Button type="primary" id="ShareGameButton__TGGames" onClick={onShare}><ShareAltOutlined/></Button>}
        </div>
        {!gameId && <ChatResults
            showTabs
            showShareBtn={false}
            title=""
            selectedSegment={selectedSegment}
            onChangeSegment={onChangeSegment}
            options={options}
            results={getCurrentResults(selectedSegment, chatResults, tgTopResults, allTopResults)}
            isLoadingResults={isLoadingResults}
        />}
        {gameId && (gameResult === GameResultEnum.isNonWin) && <Timer
            restartProp={rightField}
            startTime={AppConfig.singleGame.interval / 1000}
            onEndTime={onEndTime}/>
        }
        {gameId && <EnemyInfoBlock enemyData={enemyData} />}
        <Tour open={isOpenTour} rootClassName={styles.tourMainContentContainer} onClose={() => setIsOpenTour(false)}
              steps={steps}/>
        <ResultModal
            isOpen={gameResult !== GameResultEnum.isNonWin}
            gameResult={gameResult}
            leftField={leftField}
            rightField={rightField}
            onOk={onRestart}
            gameId={gameId}
            userId={auth.userId}
        />
        <ProfileModal
            userId={auth.userId}
            onClose={() => setIsOpenProfileModal(false)}
            isOpen={isOpenProfileModal}
        />
    </div>
}

export default TGGamePage
