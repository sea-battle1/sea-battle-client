import {api} from "../../core/api";
import {AuthTypes} from "../../core/types/authTypes";
import {TourProps} from "antd";

export const options = [
    {
        label: 'Чат',
        value: 'chatResults'
    },
    {
        label: 'Топ Телеграмма',
        value: 'topTG'
    },
    {
        label: 'Топ всех игроков',
        value: 'topAll'
    },
]

export const getCurrentResults = (selectedFragment: string, chatResults: any, tgTopResults: any, allTopResults: any) => {
    if (selectedFragment === 'chatResults') return chatResults
    if (selectedFragment === 'topTG') return tgTopResults
    if (selectedFragment === 'topAll') return allTopResults
}

export const getTGTopResults = async () => {
    const usersFromDB = await api.getUsersList({
        authType: AuthTypes.TG,
        limit: 20,
        sort: true,
    })

    return usersFromDB.map((u) => {
        return {
            user: {
                authType: u.authType,
                id: u.userId,
                is_bot: false,
                first_name: u.first_name,
                last_name: u.last_name,
                username: u.nick_name,
                // @ts-ignore
                photoUrl: u.photoUrl
            },
            score: u.singleWin + u.mpWin + u.simulateWin
        }
    })
}

export const getAllTopResults = async () => {
    const usersFromDB = await api.getUsersList({
        limit: 20,
        sort: true
    })

    return usersFromDB.map((u) => {
        return {
            user: {
                authType: u.authType,
                id: u.userId,
                is_bot: false,
                first_name: u.first_name,
                last_name: u.last_name,
                username: u.nick_name,
                // @ts-ignore
                photoUrl: u.photoUrl
            },
            score: u.singleWin + u.mpWin + u.simulateWin
        }
    })
}


export const steps: TourProps['steps'] = [
    {
        title: 'Обучение',
        description: 'Хотите пройти небольшое обучение для комфортной игры?',
        nextButtonProps: {
            children: 'Да'
        }
    },
    {
        title: 'Режимы игры',
        description: 'В игре доступны 2 режима игры. Многопользовательская игра и игра против компьютера. При старте игры вы встанете в очередь поиска противника. Если вы не хотите ждать или хотите играть против компьютера просто нажмите на кнопку играть против компьютера.',
        nextButtonProps: {
            children: 'Далее',
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Информация о ваших кораблях',
        description: 'В этом блоке отображаются корабли с вашего поля, убитые корабли будут закрашены',
        target: () => document.getElementById('FieldHeader__Player') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Игровое поле',
        description: 'В данном игровом поле располагаются ваши корабли',
        target: () => document.getElementById('Field__Self') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Автоматическая расстановка кораблей',
        placement: 'top',
        description: 'Нажмите сюда и корабли будут расставлены автоматически',
        target: () => document.getElementById('RandomShipSetterButton') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Ручная расстановка кораблей',
        placement: 'top',
        description: 'Если хотите установить корабли вручную, нажмите сюда, далее просто переносите корабли на поле',
        target: () => document.getElementById('ManualShipSetterButton') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Результаты других игроков',
        placement: 'top',
        description: 'В данном блоке отображаются результаты других игроков. Вы можете посмотреть результаты чата откуда запустили игру, лучших игроков в телеграме и на других платформах',
        target: () => document.getElementById('ResultsBlocks') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Поделитесь игрой с друзьями',
        placement: 'top',
        description: 'Нажмите что бы поделиться игрой с друзьями',
        target: () => document.getElementById('ShareGameButton__TGGames') as HTMLElement,
        nextButtonProps: {
            children: 'Далее'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
    {
        title: 'Старт игры',
        placement: 'top',
        description: 'Для начала игры нажмите кнопку Старт. Хотите начать?',
        target: () => document.getElementById('StartGameButton__TGGames') as HTMLElement,
        nextButtonProps: {
            children: 'Завершить'
        },
        prevButtonProps: {
            children: 'Назад'
        }
    },
];