import {ChatUserResultType} from "../../../../core/types/userTypes";
import {AuthTypes} from "../../../../core/types/authTypes";

export const getAvatarUrl = (user: ChatUserResultType['user'] & {authType?: AuthTypes}) => {

    if (user.photoUrl) return user.photoUrl
    if (user?.authType === AuthTypes.TG) return require('./../../../../core/assets/images/tgLogo.png')
    if (user?.authType === AuthTypes.Google) return require('./../../../../core/assets/images/googleLogo.png')

    return require('./../../../../core/assets/images/anonimeAvatar.png')
}