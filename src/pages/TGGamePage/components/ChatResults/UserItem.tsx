import React from "react";
import {Avatar, Badge} from "antd";
import {ChatUserResultType} from "../../../../core/types/userTypes";
import {getAvatarUrl} from "./utils";
import styles from './styles.module.css'

type UserItemProps = {
    user: ChatUserResultType['user'],
    score: ChatUserResultType['score']
}

export const UserItem: React.FC<UserItemProps> = ({user, score}) => {
    return <div className={styles.userItemContainer} key={user.id}>
        <Badge count={score} overflowCount={10000} showZero color="#52c41a">
            <Avatar
                size={{xs: 64, sm: 64, md: 64, lg: 64, xl: 64, xxl: 64}}
                src={getAvatarUrl(user)}
                alt="error"
            />
        </Badge>
        <span className={styles.name}>{user.first_name} {user.last_name}</span>
    </div>
}