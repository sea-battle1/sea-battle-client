import React, {useState} from "react";
import styles from './styles.module.css'
import {ChatUserResultType} from "../../../../core/types/userTypes";
import {UserItem} from "./UserItem";
import {ShareAltOutlined} from "@ant-design/icons";
import {Button, Segmented, Select, Spin, Tabs} from "antd";
import {SegmentedLabeledOption, SegmentedValue} from "antd/lib/segmented";
import {useWindowsWidth} from "../../../../core/helpers/useWindowWidth";

type ChatResultsProps = {
    results: ChatUserResultType[] | null,
    showShareBtn?: boolean,
    title?: string,
    showTabs?: boolean,
    options?: SegmentedLabeledOption[],
    selectedSegment?: SegmentedValue,
    onChangeSegment?: (val: SegmentedValue) => void,
    isLoadingResults?: boolean
}

export const ChatResults: React.FC<ChatResultsProps> = ({
                                                            results,
                                                            showTabs,
                                                            showShareBtn = true,
                                                            title = 'Результаты чата',
                                                            options,
                                                            selectedSegment,
                                                            onChangeSegment,
                                                            isLoadingResults
                                                        }) => {


    const width = useWindowsWidth()

    if (!results) return null
    const onShare = () => {
        // @ts-ignore
        window.TelegramGameProxy.shareScore()
    }

    return <div id="ResultsBlocks" className={styles.chatResultsContainer}>
            <h5 className={styles.title}>
                {title}
                {showTabs && options && selectedSegment && onChangeSegment && <Segmented
                    className={styles.segmentedContainer}
                    size={width < 600 ? "small" : 'large'}
                    options={options}
                    value={selectedSegment}
                    onChange={onChangeSegment}
                />}
                {showShareBtn && <Button type="primary" onClick={onShare}>Поделиться <ShareAltOutlined/></Button>}
            </h5>
            <Spin wrapperClassName={styles.chatResultsUsers} spinning={isLoadingResults}>
                {results.length === 0 && <h5 className={styles.noResults}>Нет результатов...</h5>}
                {results.map((res) => <UserItem key={res.user.id} user={res.user} score={res.score}/>)}
            </Spin>
        </div>
}